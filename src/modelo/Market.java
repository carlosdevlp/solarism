/*Entidad central MARKET*/
/*Entidad centrar sirve para administrar toda la data de los modelos partipantes en el contexto
mercado*/
package modelo;
import java.util.*;

/**
 *
 * @author carlos
 */
abstract public class Market {
    
    private static ArrayList<Usuario> _usuarios=new ArrayList(); //lista de usuarios en el sistema   
    private static ArrayList<Producto> _productos=new ArrayList();//inventario de productos
    private static ArrayList<Distribuidor> _distribuidores=new ArrayList();//inventario de distribuidores
    private static ArrayList<Cliente> _clientes=new ArrayList();//inventario de clientes
    private static ArrayList<Venta> _ventas=new ArrayList();//inventario de ventas
    private static ArrayList<Compra> _compras=new ArrayList();//inventario de compras
    private static ArrayList<Empleado> _empleados=new ArrayList();//lista de empleado respecto a la cantidad de usuarios

    public static void init(){
    
    }    
    //setters
    public static void setProducto(Producto _producto){
        Market._productos.add(_producto);
    }
    public static void setProductos(ArrayList<Producto> _productos){
        Market._productos=_productos;
    }        
    public static void setDistribuidores(ArrayList<Distribuidor> _distribuidores) {
        Market._distribuidores = _distribuidores;
    }
    public static void setCliente(ArrayList<Cliente> _clientes){
        Market._clientes= _clientes;
    }
    public static void setVentas(ArrayList<Venta> _ventas) {
        Market._ventas = _ventas;
    }
    public static void setCompras(ArrayList<Compra> _compras) {
        Market._compras = _compras;
    }
    public static void setEmpleados(ArrayList<Empleado> _empleados) {
        Market._empleados = _empleados;
    }
    //getters
    public static ArrayList<Producto> getProductos() {
        return _productos;
    }
    public static ArrayList<Usuario> getUsuarios() {
        return _usuarios;
    }    
    public static ArrayList<Distribuidor> getDistribuidores() {
        return _distribuidores;
    }    
    public static ArrayList<Cliente> getClientes(){
        return _clientes;
    }    
    public static ArrayList<Venta> getVentas() {
        return _ventas;
    }    
    public static ArrayList<Compra> getCompras() {
        return _compras;
    }
    public static ArrayList<Empleado> getEmpleados() {
        return _empleados;
    }
    //others
    //adds
    public static void addUsuario(Usuario _usuario){
        Market._usuarios.add(_usuario);
    }
    public static void addProducto(Producto _producto){
        Market._productos.add(_producto);
    }
    public static void addDistribuidor(Distribuidor _distribuidor){
        Market._distribuidores.add(_distribuidor);
    }
    public static void addCliente(Cliente _cliente){
        Market._clientes.add(_cliente);
    }
    public static void addVenta(Venta _venta){
        Market._ventas.add(_venta);
    }    
    public static void addCompra(Compra _compra){
        Market._compras.add(_compra);
    }
    public static void addEmpleado(Empleado _empleado) {
        Market._empleados.add(_empleado);
    }
    
    //create
    public static void createUsuarios(ArrayList<ArrayList<String>> _str){
        Market._usuarios.clear();
        for(ArrayList<String> row: _str)          
             Market._usuarios.add(new Usuario(row.get(0),row.get(1),row.get(2)));
    }
    
    public static void createProductos(ArrayList<ArrayList<String>> _str){        
        Market._productos.clear();        
        for(ArrayList<String> row: _str){  
            Producto.generarID(Integer.parseInt(row.get(0)));
            Market._productos.add(new Producto(Integer.parseInt(row.get(0)),row.get(1),Integer.parseInt(row.get(2)),Double.parseDouble(row.get(3)),Double.parseDouble(row.get(4)) ) );
        }
    }
    public static void createDistribuidores(ArrayList<ArrayList<String>> _str){
        Market._distribuidores.clear();        
         for(ArrayList<String> row: _str){    
             Distribuidor.generarID(Integer.parseInt(row.get(0)));
             Market._distribuidores.add(new Distribuidor(Integer.parseInt(row.get(0)),row.get(1),row.get(2)));
         }
    }
    public static void createClientes(ArrayList<ArrayList<String>> _str){  
        Market._clientes.clear();
        int telefono;
         for(ArrayList<String> row: _str){
             //Market._clientes.add(new Cliente());new Cliente(row.get(0),row.get(1),row.get(2),row.get(3),row.get(4),row.get(5),Integer.parseInt(row.get(6))));
             Cliente.generarID(Integer.parseInt(row.get(0)));
             try{telefono=Integer.parseInt(row.get(3));}catch(Exception err){telefono=0;}
             Market._clientes.add(new Cliente(Integer.parseInt(row.get(0)),row.get(1),row.get(2),telefono));
         }
    }
    public static void createVentas(ArrayList<ArrayList<String>> _str){
        Market._ventas.clear();
        for(ArrayList<String> row: _str){
            Venta.generarID(Integer.parseInt(row.get(0)));
            Market._ventas.add(new Venta(Integer.parseInt(row.get(0)),row.get(1),row.get(2),row.get(3),Integer.parseInt(row.get(4)),row.get(5)) );
        }
    }
    public static void createCompras(ArrayList<ArrayList<String>> _str){
        Market._compras.clear();
        for(ArrayList<String> row: _str)
            Market._compras.add(new Compra(Integer.parseInt(row.get(0)),row.get(1),row.get(2),row.get(3),Integer.parseInt(row.get(4)),row.get(5)));
    }
    public static void createEmpleados(ArrayList<ArrayList<String>> _str){
        Market._empleados.clear();
        int telefono;        
        for(ArrayList<String> row: _str){
            Empleado.generarID(Integer.parseInt(row.get(0)));
            try{telefono=Integer.parseInt(row.get(5));}catch(Exception err){telefono=0;}
            Market._empleados.add(new Empleado(Integer.parseInt(row.get(0)),row.get(1),row.get(2),row.get(3),row.get(4),telefono,new Usuario(row.get(6),row.get(7),row.get(8))));
        }
    }
    //replace
    public static void replaceUsuario(int _ind,Usuario _usuario){        
        Market._usuarios.set(_ind, _usuario);
    }    
    public static void replaceProducto(int _ind,Producto _producto){        
        Market._productos.set(_ind, _producto);
    }    
    public static void replaceDistribuidor(int _ind,Distribuidor _distribuidor){        
        Market._distribuidores.set(_ind, _distribuidor);
    }
    public static void replaceCliente(int _ind,Cliente _cliente){        
        Market._clientes.set(_ind, _cliente);
    }
    public static void replaceEmpleado(int _ind,Empleado _empleado){        
        Market._empleados.set(_ind, _empleado);
    }
    //remove
    public static void removeUsuario(int ind){
        Market._usuarios.remove(ind);        
    }            
    public static void removeProducto(int ind){
        Market._productos.remove(ind);
    }
    public static void removeDistribuidor(int ind){
        Market._distribuidores.remove(ind);
    }
    public static void removeCliente(int ind){
        Market._clientes.remove(ind);
    }
    public static void removeEmpleado(int ind){
        Market._empleados.remove(ind);
    }
    //remove where CODIGO
    public static void removeProductoWhere(String codigo){
        for(int i=0;i<Market._productos.size();i++)            
            if(Market._productos.get(i).getSCodigo().compareTo(codigo)==0)
                Market._productos.remove(i);
    }
    
    public static void removeClienteWhere(String codigo){
        for(int i=0;i<Market._clientes.size();i++)            
            if(Market._clientes.get(i).getSCodigo().compareTo(codigo)==0)
                Market._clientes.remove(i);
    }
    
    //edit
    public static void editProducto(int _ind,Vector<String> _producto){        
        Market._productos.get(_ind).set(_producto);
    }
    public static void editCliente(int _ind,Vector<String> _cliente){        
        Market._clientes.get(_ind).set(_cliente);
    }                
    //getter de atributo
    public static String getProductoProperty(String codigo,int ind){
        Vector<String> str;
        for(int i=0;i<Market._productos.size();i++)
            if(Market._productos.get(i).getSCodigo().compareTo(codigo)==0){
                    str=Market._productos.get(i).toVector();
                    return str.get(ind);
            }
        return "";
    }
    public static String getClienteProperty(String codigo,int ind){
        Vector<String> str;
        for(int i=0;i<Market._clientes.size();i++)
            if(Market._clientes.get(i).getSCodigo().compareTo(codigo)==0){
                    str=Market._clientes.get(i).toVector();
                    return str.get(ind);
            }
        return "";
    }
    //getter de atributos
    public static Vector getProductoProperties(String codigo){        
                for(int i=0;i<Market._productos.size();i++)
                    if(Market._productos.get(i).getSCodigo().compareTo(codigo)==0)
                            return Market._productos.get(i).toVector();
                
           return null;
    }
    
    public static Vector getClienteProperties(String codigo){        
                for(int i=0;i<Market._clientes.size();i++)
                    if(Market._clientes.get(i).getSCodigo().compareTo(codigo)==0)
                            return Market._clientes.get(i).toVector();
                
           return null;
    }
    /*public static void editDistribuidor(int _ind,Vector<String> _producto){        
        Market._distribuidores.get(_ind).set(_producto);
    } */   
}
