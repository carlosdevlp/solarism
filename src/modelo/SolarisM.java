/*Plantilla SolarisM para generar polimorfismo*/
package modelo;
import java.util.Vector;

/**
 *
 * @author carlos
 */
public interface SolarisM {
        //public void set(String []str,int []num,Double []dec);opción 1
        //public void set(String []str);opción 2
        public void set(Vector<String> str);//setter genérico opción 3
        public Vector toVector();
        public SolarisM clone();
        public void setCodigo(int _codigo);
        public int getCodigo();//código en número
        public String getSCodigo();//código en string
        public boolean filtro(String tag,String str1);//filtro para búsqueda
        public void query(String tipoConsulta);//ejecutar query
}  
