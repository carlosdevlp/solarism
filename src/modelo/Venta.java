/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;
import java.util.*;
/**
 *
 * @author carlos
 */
public class Venta extends Operacion implements SolarisM{
    private static int id=0;
    private String _codigoCliente,_estado;
    private Double _montoTotal;
    
    //constructor por defecto
    public Venta(){
        
    }
    //constructor con parámetros
    public Venta(int _codigo,String _codigoEmpleado, String _codigoCliente, String _codigoProductos,int _cantidad, String _fecha, String _estado) {
        super(_codigo, _cantidad,_codigoEmpleado, _codigoProductos, _fecha,"VENT");
        this._codigoCliente =_codigoCliente;     
        this._estado = _estado;
        this._montoTotal=0.0;
    }
    //para recibirlo desde la base de datos
    public Venta(int _codigo,String _codigoEmpleado, String _codigoCliente, String _codigoProductos,int _cantidad, String _fecha) {
        super(_codigo, _cantidad,_codigoEmpleado, _codigoProductos,_fecha,"VENT");  
        this._codigoCliente = _codigoCliente;
        
    }
    //para crearlo desde el programa
    public Venta(int _codigo,String _codigoEmpleado, String _codigoCliente, ArrayList<String> _codigosProductos, ArrayList<String> _cantidades, double _montoTotal){
        super(_codigo,_codigoEmpleado,_codigosProductos,_cantidades,Util.format.currentDate(),"VENT");
        this._codigoCliente = _codigoCliente;   
    }
    
    public Venta(String _codigoEmpleado, String _codigoCliente, ArrayList<String> _codigosProductos, ArrayList<String> _cantidades, double _montoTotal){
        super(Venta.id,_codigoEmpleado,_codigosProductos,_cantidades,Util.format.currentDate(),"VENT");
        this._codigoCliente = _codigoCliente;   
    }
    //setter    
      //super set
        public void set(Vector<String> str){
            this.setCodigo(Integer.parseInt(str.get(0)));
            this.setCantidad(Integer.parseInt(str.get(1)));
            this.setCodigoEmpleado(str.get(2));
            this._codigoCliente = str.get(3);
        }    
    public static void setID(int id){
        Venta.id=id;
    }   
     
    public void setCodigoClientes(String _codigoClientes) {
        this._codigoCliente = _codigoClientes;
    }
    public void setEstado(String _estado) {
        this._estado = _estado;
    }
    //getter
    public static int getID(){
        return Venta.id;
    }
    public String getCodigoClientes() {
        return _codigoCliente;
    }
    public String getEstado() {
        return _estado;
    }
    //clonación
    public SolarisM clone(){    
        return new Venta( getCodigo(),getCodigoEmpleado(),  _codigoCliente,getCodigoProductos(), getCantidad(), getFecha());       
    }
    //filtro para búsqueda
    public boolean filtro(String tag,String str1){
        String str2;        
        switch(tag){
            case "IDCLIENTE":
                str2="CLIENT"+this._codigoCliente;
            break;
            case "IDPRODUCTO":
                str2="PROD"+this.getCodigoProductos();
            break;
            case "FECHA":
                str2=this.getFecha();
            break;
            default:
                str2=this.getSCodigo();
            
        }
        return str2.compareTo(str1)==0;
    }
    //ejecutar query
    public void query(String tipoConsulta){
        //Vector aux=this.toVector();
        switch(tipoConsulta){
            case "INSERT":                                                
                //inserción de la venta de cada producto                
                    Dao.insert("IDVENTAS,IDPRODUCTOS,CANTIDAD", new String[]{this.getCodigo()+"",this.getCodigoProductos(),this.getCantidad()+""}, "PRODUCTOS_VENTAS");  
                    Market.addVenta(this);
            break;
            case "UPDATE":
                
            break;
            case "DELETE":
                
            break;
        }
    }
    //hacer un query solo para la tabla ventas
    public static void query(String tipoConsulta,String _codigoEmpleado,String _codigoCliente,String _fecha,String _montoTotal){
        switch(tipoConsulta){
            case "INSERT"://insercion de la venta
                String Id=Venta.id+"";                
                Dao.insert("IDEMPLEADO,IDCLIENTES,FECHA,COSTOTOTAL", new String[]{_codigoEmpleado,_codigoCliente,_fecha,_montoTotal}, "VENTAS");
            break;
            case "UPDATE":
                
            break;
            case "DELETE":
                
            break;
        }
    }
    //TYPECAST
    public Vector toVector(){
        Vector<String> vct= new Vector();
            vct.add(this.getCodigo()+"");
            vct.add("CLIENT"+this._codigoCliente);
            vct.add("PROD"+this.getCodigoProductos());
            vct.add(+this.getCantidad()+"");
            vct.add(this.getFecha());
            //vct.add(this.getCodigoEmpleado());        
        return vct; 
    }

    @Override
    public String toString() {
        return "-Venta- " +super.toString() +"_codigoClientes=" + _codigoCliente + ", _estado=" + _estado;
    }
    //ID 
    public static int sgteID(){       
        id++;
        return id;
    }   
    //generar ID seteando los valores que se ingresan y cogiendo el mayor de ellos
    public static void generarID(int id){
        if(id>Venta.id)
            Venta.id=id;    
    }
    
}
