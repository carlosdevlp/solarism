/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.util.Vector;

/**
 *
 * @author carlos
 */
public class Compra extends Operacion implements SolarisM{
    private String _codigoProveedor;
    //constructor por defecto
    public Compra(){
        
    }
    //constructor con parámetros    
    public Compra(int _codigo, String _codigoEmpleado, String _codigoProveedor, String _codigoProductos,int _cantidad,  String _fecha) {
        super(_codigo, _cantidad,_codigoEmpleado,_codigoProductos, _fecha,"COMP");
        this._codigoProveedor =_codigoProveedor;                        
    }
    
    //setter    
      //super set
        public void set(Vector<String> str){
            this.setCodigo(Integer.parseInt(str.get(0)));
            this.setCantidad(Integer.parseInt(str.get(1)));
            this.setCodigoEmpleado(str.get(2));
            this._codigoProveedor = str.get(3);
        }    
    public void setCodigoProveedor(String _codigoProveedor) {
        this._codigoProveedor = _codigoProveedor;
    }    
    //getter
    public String getCodigoProveedor() {
        return this._codigoProveedor;
    }
    
    //clonación
    public SolarisM clone(){    
        return new Compra( getCodigo(),  getCodigoEmpleado(), _codigoProveedor, getCodigoProductos(), getCantidad(),getFecha());       
    }
    //filtro para búsqueda
    public boolean filtro(String tag,String str1){
        String str2;        
        switch(tag){
            case "IDPROVEEDOR":
                str2="PROV"+this._codigoProveedor;
            break;
            case "IDPRODUCTO":
                str2="PROD"+this.getCodigoProductos();
            break;
            case "FECHA":
                str2=this.getFecha();
            break;
            default:
                str2=this.getSCodigo();
            
        }
        System.out.println(tag+"- str1: "+str1+"str2: "+str2);
        return str2.compareTo(str1)==0;
    }
    //ejecutar query
    public void query(String tipoConsulta){
        Vector aux=this.toVector();
        switch(tipoConsulta){
            case "INSERT":
                
            break;
            case "UPDATE":
                
            break;
            case "DELETE":
                
            break;
        }
    }
    
    //TYPECAST
    public Vector toVector(){
        Vector<String> vct= new Vector();
            vct.add(this.getCodigo()+"");                        
            vct.add("PROV"+this._codigoProveedor);
            vct.add("PROD"+this.getCodigoProductos());
            vct.add(this.getCantidad()+"");
            vct.add(this.getFecha());
            //vct.add(this.getCodigoEmpleado());        
        return vct; 
    }
    
}
