/*Entidad Empleado */
package modelo;
import java.util.*;
/**
 *
 * @author carlos
 */
public class Empleado  extends Persona implements SolarisM{
    private static int id=0;
    private int _codigo,_codPersona;
    private String _cargo;
    private Usuario _usuario;
    //constructor sin parámetros
    public Empleado(){
    
    }
    //constructor con parámetros
    public Empleado(int _codigo,String _nombre,String _cargo, Usuario _usuario,String _dni, String _direccion, String _idDistrito, String _idProvincia, String _idDepartamento, int _telefono){
        super(_nombre,  _dni, _direccion, _idDistrito, _idProvincia, _idDepartamento, _telefono);
        this._codigo=_codigo;
        this._cargo=_cargo;
        this._usuario=_usuario;
    }
    public Empleado(int _codigo,String _nombre,String _cargo,String _dni, String _direccion, int _telefono,Usuario _usuario){
        super(_nombre,  _dni, _direccion, "vacío","vacío","vacío", _telefono);
        this._codigo=_codigo;
        this._cargo=_cargo;        
        this._usuario=_usuario;
        this._usuario.setCodigo(_codigo);
        
    }
    //setters
        //super set
        public void set(Vector<String> str){
            this._codigo=Integer.parseInt(str.get(0));
            this.setNombre(str.get(1));
            this._cargo=str.get(2);
            this.setDni(str.get(3));  
            this.setDireccion(str.get(4));
            this.setTelefono(Integer.parseInt(str.get(5)));
            this._usuario.setUsername(str.get(6));
            this._usuario.setPassword(str.get(7));
            this._usuario.setTipo(str.get(8));
        }
    public static void setID(int id){  
        Empleado.id=id;
    }
    public void setCodigo(int _codigo){
          this._codigo=_codigo;
    }
    public void setCargo(String _cargo) {
        this._cargo = _cargo;
    }
    public void setUsuario(Usuario _usuario) {
        this._usuario = _usuario;
    }
    
    
    //getters
    public static int getID(){        
        return id;
    }   
    public String getSCodigo(){
        return "EMP"+_codigo;
    }
    public int getCodigo(){
        return _codigo;
    }
    public String getCargo() {
        return _cargo;
    }
    public Usuario getUsuario() {
        return _usuario;
    }
    //filtro para búsqueda
     public boolean filtro(String tag,String str1){
         String str2;        
            switch(tag){
                case "NOMBRE":
                    str2=this.getNombre().toUpperCase();
                break;
                case "CARGO":
                    str2=this._cargo.toUpperCase();
                break;
                case "DNI":
                    str2=this.getDni();
                break;
                case "TELÉFONO":
                    str2=""+this.getTelefono();
                break;
                default:
                    str2=this.getSCodigo();
            }

            return str2.compareTo(str1)==0;                  
     }
     //ejecutar query
     public void query(String tipoConsulta){
          ArrayList<ArrayList<String>> aux;
            
        switch(tipoConsulta){
            case "INSERT":     
                Dao.insert("NOMBRE,DNI,TELEFONO,DIRECCION", new String[]{this.getNombre(),this.getDni(),this.getTelefono()+"",this.getDireccion()}, "PERSONA");                
                        aux=Dao.select("IDPERSONA","PERSONA", "DNI='"+this.getDni()+"'");
                Dao.insert("IDEMPLEADO,CARGO,IDPERSONA",new String[]{this._codigo+"",this._cargo,aux.get(0).get(0)} ,"EMPLEADO");
                Market.addEmpleado(this);
                this._usuario.query(tipoConsulta);
            break;
            case "UPDATE":
                Dao.update("CARGO",new String[]{this._cargo},"EMPLEADO","IDEMPLEADO = "+this._codigo);
                Dao.update("NOMBRE,DNI,TELEFONO,DIRECCION", new String[]{this.getNombre(),this.getDni(),this.getTelefono()+"",this.getDireccion()},"PERSONA", "IDPERSONA = (SELECT IDPERSONA FROM EMPLEADO WHERE IDEMPLEADO= "+this._codigo+" )");
                this._usuario.query(tipoConsulta);
            break;
            case "DELETE":
                this._usuario.query(tipoConsulta);
                    aux=Dao.select("IDPERSONA","EMPLEADO", "IDEMPLEADO="+this._codigo);
                Dao.delete("EMPLEADO","IDEMPLEADO = "+this._codigo);
                Dao.delete("PERSONA","IDPERSONA = "+aux.get(0).get(0));
                Market.removeProductoWhere(this.getSCodigo());
            break;
        }
        
     }
    //clonación
    public SolarisM clone(){
        return new Empleado();
    }
    //typecast
    public Vector toVector(){
        Vector vct=new Vector();
        vct.add(""+this._codigo);
        vct.add(this.getNombre());        
        vct.add(this._cargo);
        vct.add(this.getDni());        
        vct.add(this.getDireccion());
        vct.add(this.getTelefono()+"");
        vct.add(this._usuario.getUsername());
        vct.add(this._usuario.getPassword());
        vct.add(this._usuario.getTipo());
        
        
        return vct;
    }
   //ID 
    public static int sgteID(){       
        id++;
        return id;
    }   
    //generar ID seteando los valores que se ingresan y cogiendo el mayor de ellos
    public static void generarID(int id){
        if(id>Empleado.id)
            Empleado.id=id;    
    }
    
}
