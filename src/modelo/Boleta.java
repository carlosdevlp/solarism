/*Entidad Boleta */
package modelo;

/**
 *
 * @author carlos
 */
import java.util.*;

public class Boleta implements SolarisM{
    private static final String []estado= {"PENDIENTE","CANCELADO"};
    private static int id=1;
    private int _codigo;
    private String _codigoCliente,_estado,_tipo;
    private ArrayList<String> _codigoProducto,_cantidad;
    private double _montoTotal;
    private Date _fecha;
    //constructor por defecto
    public Boleta(){        
        
    }
    //constructor por parámetros
    public Boleta(int _codigo, String _codigoCliente, ArrayList<String> _codigoProducto, ArrayList<String> _cantidad, double _montoTotal,String _tipo) {
        this._fecha=new Date();
        this._codigo = _codigo;
        this._codigoCliente = _codigoCliente;        
        this._codigoProducto = _codigoProducto;
        this._cantidad = _cantidad;
        this._montoTotal = _montoTotal;
        this._tipo=_tipo;
    }
    
    public Boleta(int _codigo, String _codigoCliente, String _estado, ArrayList<String> _codigoProducto, ArrayList<String> _cantidad, double _montoTotal) {
        this._codigo = _codigo;
        this._codigoCliente = _codigoCliente;
        this._estado = _estado;
        this._codigoProducto = _codigoProducto;
        this._cantidad = _cantidad;
        this._montoTotal = _montoTotal;
    }

    
    
    

    
    //setear el estado de boleta si esta  pendiente(0) o cancelado(1)
    //SETTERS 
        //super set
        public void set(Vector<String> str){
        
        }
    public static void setID(int id){
        Boleta.id=id;
    }
    public void setCodigo(int _codigo) {
        this._codigo = _codigo;
    }
    public void setEstado(int est) {
        this._estado = Boleta.estado[est];
    }        

    public void setCodigoCliente(String _codigoCliente) {
        this._codigoCliente = _codigoCliente;
    }

    public void setCodigoProducto(ArrayList<String> _codigoProducto) {
        this._codigoProducto = _codigoProducto;
    }

    public void setCantidad(ArrayList<String> _cantidad) {
        this._cantidad = _cantidad;
    }

    public void setMontoTotal(double _montoTotal) {
        this._montoTotal = _montoTotal;
    }
    
    //GETTERS
    public static int getID(){
        return id;
    }
    public int getCodigo() {
        return _codigo;
    }
    public String getSCodigo(){//código en string        
        return "BOL"+this._codigo;
    }

    public String getCodigoCliente() {
        return _codigoCliente;
    }

    public String getEstado() {
        return _estado;
    }

    public ArrayList<String> getCodigoProducto() {
        return _codigoProducto;
    }

    public ArrayList<String> getCantidad() {
        return _cantidad;
    }

    public double getMontoTotal() {
        return _montoTotal;
    }
     
    //filtro para búsqueda
    public boolean filtro(String tag,String str1){
        return false;
    }
    //ejecutar query    
    public void query(String tipoConsulta){
       Vector aux=this.toVector();
        switch(tipoConsulta){
            case "INSERT":
                switch(this._tipo){
                    case "VENTA"://realizar una venta
                        Dao.insert("IDEMPLEADO,IDCLIENTES,FECHA,COSTOTAL", new String[]{"",this._codigoCliente,this._montoTotal+""}, "VENTAS");
                        for(int i=0;i<this._codigoProducto.size();i++)
                            Dao.insert("IDVENTAS,IDPRODUCTOS,CANTIDAD", new String[]{Boleta.id+"",this._codigoProducto.get(i),this._cantidad.get(i)}, "PRODUCTOS_VENTAS");
                        Boleta.id++;
                    break;
                    case "COMPRA":
                    break;
                }
            break;
            case "UPDATE":
                switch(this._tipo){
                    case "VENTA":
                    break;
                    case "COMPRA":
                    break;
                }
            break;
            case "DELETE":
                switch(this._tipo){
                    case "VENTA":
                    break;
                    case "COMPRA":
                    break;
                }
            break;
        }
    }
    //clonación 
    public SolarisM clone(){
        return new Boleta();
    }    
    
    //calcular el monto total a ser publicado 
    /*public double calcularMontoTotal(){
        double total=0.0;
        for (Compras c: _compras)
            total+=c.calcularMonto();
        return total;
    }*/
    //typecast 
    public Vector toVector(){
        Vector<String> vct=new Vector();
            
        return vct;
    }
    //generar ID seteando los valores que se ingresan y cogiendo el mayor de ellos
    
}
