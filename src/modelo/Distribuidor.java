/*Entidad Distribuidor */
package modelo;
import java.util.*;
/**
 *
 * @author carlos
 */
public class Distribuidor extends Empresa implements SolarisM{
    private static int id=0;
    private int _codigo;    
    
    public Distribuidor() {
        super("","");
    }
    public Distribuidor(int _codigo,String _nombre, String _ruc) {
        super(_nombre,_ruc);
        this._codigo=_codigo;        
    }
    //setters
        //super set
        public void set(Vector<String> str){
            this._codigo=Integer.parseInt(str.get(0));
            super.setNombre(str.get(1));
            super.setRuc(str.get(2));
        }                
    public static void setID(int id) {
        Distribuidor.id = id;
    }
    public void setCodigo(int _codigo){
        this._codigo=_codigo;
    }
    
    //getters
    public static int getID() {
        return id;
    }       
    public int getCodigo(){
        return _codigo;
    }
    public String getSCodigo(){
        return "DIST"+_codigo;
    }
    //filtro para búsqueda - filtros aceptados (nombre,código,RUC)
    public boolean filtro(String tag,String str1){
        String str2;        
        switch(tag){
            case "NOMBRE":
                str2=this.getNombre().toUpperCase();
            break;
            case "RUC":
                str2=this.getRuc().toUpperCase();
            break;
            default:
                str2=this.getSCodigo();
        }
                        
        return str2.compareTo(str1)==0;
    }    
    //clonación
    public SolarisM clone(){
        return new Distribuidor(this._codigo,this.getNombre(),this.getRuc());
    }
    //BASE DE DATOS
    //ejecución del queries
    public void query(String tipoConsulta){
        Vector aux=this.toVector();
        switch(tipoConsulta){
            case "INSERT":
                Distribuidor nDistribuidor=new Distribuidor();
                nDistribuidor.set(aux);
                Market.addDistribuidor(nDistribuidor);
                Dao.insert("IDDISTRIBUIDORES,NOMBRE,RUC",aux ,"DISTRIBUIDORES");
            break;
            case "UPDATE":
                Dao.update("IDDISTRIBUIDORES,NOMBRE,RUC",aux,"DISTRIBUIDORES","IDDISTRIBUIDORES = "+aux.get(0) );
            break;
            case "DELETE":
                Market.removeProductoWhere(this.getSCodigo());
                Dao.delete("DISTRIBUIDORES","IDDISTRIBUIDORES= "+this._codigo);
            break;
        }
        
    }
    //typecast
    public Vector toVector(){
         Vector<String> vct=new Vector();
        
        vct.add(this._codigo+"");
        vct.add(this.getNombre());
        vct.add(this.getRuc());
        
        return vct;
    }
    //ID 
    public static int sgteID(){       
        id++;
        return id;
    }   
    //generar ID seteando los valores que se ingresan y cogiendo el mayor de ellos
    public static void generarID(int id){
        if(id>Distribuidor.id)
            Distribuidor.id=id;    
    }
    
}
