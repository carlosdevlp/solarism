/*Entidad Operación */
package modelo;
import java.util.*;
/**
 *
 * @author carlos
 */
abstract public class Operacion {
    private int _codigo,_cantidad;
    private String _codigoEmpleado,_codigoProductos;
    private ArrayList<String> _codigosProductos;
    private ArrayList<String> _cantidades;
    private String _fecha;
    private String _codAbrv;
    public  Operacion(){        
    }

    public Operacion(int _codigo, int _cantidad, String _codigoEmpleado, String _codigoProductos, String _fecha,String _codAbrv) {
        this._codigo = _codigo;
        this._cantidad = _cantidad;
        this._codigoEmpleado = _codigoEmpleado;
        this._codigoProductos = _codigoProductos;
        this._fecha = _fecha;
        this._codAbrv=_codAbrv;        
    }

    public Operacion(int _codigo, String _codigoEmpleado, ArrayList<String> _codigosProductos, ArrayList<String> _cantidades, String _fecha, String _codAbrv) {
        this._codigo = _codigo;
        this._codigosProductos = _codigosProductos;
        this._codigoEmpleado = _codigoEmpleado;
        this._cantidades = _cantidades;
        this._fecha = _fecha;
        this._codAbrv = _codAbrv;
    }
    
    
    //setter
    public void setCodigo(int _codigo) {
        this._codigo = _codigo;
    }
    public void setCantidad(int _cantidad) {
        this._cantidad = _cantidad;
    }
    public void setCodigoEmpleado(String _codigoEmpleado) {
        this._codigoEmpleado = _codigoEmpleado;
    }    
    public void setCodigoProductos(String _codigoProductos) {
        this._codigoProductos = _codigoProductos;
    }
    
    public void setFecha(String _fecha) {
        this._fecha = _fecha;
    }

    public void setCodigosProductos(ArrayList<String> _codigosProductos) {
        this._codigosProductos = _codigosProductos;
    }

    public void setCantidades(ArrayList<String> _cantidades) {
        this._cantidades = _cantidades;
    }
    
    //getter
    public String getSCodigo() {
        return this._codAbrv+_codigo;
    }
    public int getCodigo() {
        return _codigo;
    }
    public int getCantidad() {
        return _cantidad;
    }
    public String getCodigoEmpleado() {
        return _codigoEmpleado;
    }
    
    public String getCodigoProductos() {
        return _codigoProductos;
    }
    
    public String getFecha() {
        return _fecha;
    }

    public ArrayList<String> getCodigosProductos() {
        return _codigosProductos;
    }

    public ArrayList<String> getCantidades() {
        return _cantidades;
    }
    
    //typecast

    @Override
    public String toString() {
        return "-Operacion-" + "_codigo=" + _codigo + ", _cantidad=" + _cantidad + ", _codigoEmpleado=" + _codigoEmpleado + ", _codigoProductos=" + _codigoProductos + ", _fecha=" + _fecha + '-';
    }
    
}
