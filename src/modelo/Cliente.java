/*Entidad Cliente*/
package modelo;

import java.util.*;

/**
 *
 * @author carlos
 */
public class Cliente extends Persona implements SolarisM {
    private static int id=0;
    private static final String []estado= {"DEUDOR","NORMAL"};
    private String _estado,_tipo;
    private int _codigo;
    private double _credito;
    
    public Cliente(){
        //función por defecto para su uso en factories
        super();
        this._codigo=0;
        this.setNombre("");        
        this._tipo="";
        this.setDni("");
        this.setTelefono(0);
        this.setDireccion("");
    }
    //cuando el cliente solo quiere ingresar datos básicos
    public Cliente(int _codigoPersona,int _codigo,String _nombre,String _dni,int _telefono){
        super(_nombre,  _dni, "vacío","vacío","vacío", "vacío",_telefono);        
        super.setCodigo(_codigoPersona);        
        this._codigo=_codigo;
        this._tipo="vacío";
    
    }
    
    public Cliente(int _codigo,String _nombre,String _dni,int _telefono){
        super(_nombre,  _dni, "vacío","vacío","vacío", "vacío",_telefono);        
        this._codigo=_codigo;
        this._tipo="vacío";
    
    }
    //cuando el cliente quiere dejar muchos datos
    Cliente(int _codigo,String _nombre,String _dni, String _direccion, String _idDistrito, String _idProvincia, String _idDepartamento, int _telefono){
      super(_nombre,  _dni, _direccion, _idDistrito, _idProvincia, _idDepartamento, _telefono);
      this._codigo=_codigo;
      this._credito=0.0;
      this._estado=Cliente.estado[0];
              
    }
    //setters
        //super set
        public void set(Vector<String> str){//setter genérico       
            this._codigo=Integer.parseInt(str.get(0));
                       
            if(str.size()>4){
                set(new String[]{str.get(1),str.get(2),str.get(3),str.get(4),str.get(5),str.get(6),str.get(7)});
                this._tipo=str.get(8);
            }
            else{ 
                set(new String[]{str.get(1),str.get(2),"vacío","vacío","vacío", "vacío", str.get(3)});
                this._tipo="vacío";
            }
            
        }
        
    public static void setID(int id){
       Cliente.id=id;
    }  
    public void setCodigo(int _codigo){
        this._codigo=_codigo;
    }
    //getters        
    public static int getID() {
        return id;
    }     
     public int getCodigo(){//código en número
         return _codigo;
     }
     public String getSCodigo(){ //código en string
         return "CLIENT"+_codigo;
     }
    //clonación
    public SolarisM clone(){
        return new Cliente(_codigo,getNombre(),getDni(),getTelefono());//,getDireccion(),getIdDistrito(),getIdProvincia(),getIdDepartamento(),getTelefono());
     }
    
    //filtro para búsqueda - filtros aceptados (nombre,código)
    public boolean filtro(String tag,String str1){
        
        String str2;        
        switch(tag){
            case "NOMBRE":
                str2=this.getNombre().toUpperCase();
            break;
            case "DNI":
                str2=this.getDni().toUpperCase();
            break;
            default:
                str2=this.getSCodigo();
        }
                        
        return str2.compareTo(str1)==0;
    }
    //setear crédito al cliente
    public void setCredito(double _credito) {
        this._credito = _credito;
    }
    
    //BASE DE DATOS
    //ejecución del queries
    public void query(String tipoConsulta){
        Vector aux=this.toVector();//nombre,dni,telefono        
        ArrayList<ArrayList<String>> aux2;//idPersona
        aux.remove(0);
        switch(tipoConsulta){
            case "INSERT"://doble insert: 1-clientes 2-persona                                
                Dao.insert("NOMBRE,DNI,TELEFONO", aux, "PERSONA");                    
                    aux2=Dao.select("IDPERSONA","PERSONA", "DNI='"+this.getDni()+"'");
                Dao.insert("IDCLIENTES,IDPERSONA",new String []{this._codigo+"",aux2.get(0).get(0)}, "CLIENTES");
            break;
            case "UPDATE":                                  
                Dao.update("NOMBRE,DNI,TELEFONO", aux,"PERSONA", "IDPERSONA = (SELECT IDPERSONA FROM CLIENTES WHERE IDCLIENTES = "+this._codigo+" )");
            break;
            case "DELETE":                
                aux2=Dao.select("IDPERSONA","CLIENTES", "IDCLIENTES="+this._codigo);
                Dao.delete("CLIENTES","IDCLIENTES = "+this._codigo);                    
                Dao.delete("PERSONA","IDPERSONA = "+aux2.get(0).get(0));
            break;
        }
        aux2=null;
    }
    
    //typecast
    public Vector toVector(){
         Vector<String> vct=new Vector();
        
        vct.add(this._codigo+"");
        vct.add(this.getNombre());
        vct.add(this.getDni());
        vct.add((this.getTelefono()==0)?"vacío":this.getTelefono()+"");
        /*vct.add(this.getDireccion());
        vct.add(this.getIdDistrito());
        vct.add(this.getIdProvincia());
        vct.add(this.getIdDepartamento());*/
        
        
        return vct;
    }
    
    //ID
    public static int sgteID(){       
        id++;
        return id;
    }   
    //generar ID seteando los valores que se ingresan y cogiendo el mayor de ellos
    public static void generarID(int id){
        if(id>Cliente.id)
            Cliente.id=id;    
    }    
}
