/*Entidad Producto */
package modelo;
import java.util.*;
/**
 *
 * @author carlos
 */
public class Producto implements SolarisM{
    private static int id=0;
    private String _nombre;//podemos agregar una categoria de productos
    private int _cantidad,_codigo;//cantidad de productos disponibles en la tienda
    private double _precioCosto,_precioVenta;
    
    public Producto(){
        //cuando se crea desde una fábrica        
    }    
    public Producto(String _nombre){        
        this._nombre = _nombre;
    }
    public Producto(int _codigo,String _nombre, int _cantidad, double _precioCosto, double _precioVenta) {         
        this._codigo=_codigo;
        this._nombre = _nombre;
        this._cantidad = _cantidad;
        this._precioCosto = _precioCosto;
        this._precioVenta = _precioVenta;
    }
     
    //SETTERS            
        //super set
        public void set(Vector<String> str){
            this._codigo=Integer.parseInt(str.get(0));
            this._nombre = str.get(1);
            this._cantidad = Integer.parseInt(str.get(2));
            this._precioCosto = Double.parseDouble(str.get(3));
            this._precioVenta = Double.parseDouble(str.get(4));
        }
    
    public static void setID(int id){
        Producto.id=id;
    }    
    public void setCodigo(int _codigo){
        this._codigo=_codigo;
    }
    public void setCantidad(int _cantidad) {
        this._cantidad = _cantidad;
    }
    public void setNombre(String _nombre) {
        this._nombre = _nombre;
    }
    public void setPrecioCosto(double _precioCosto) {
        this._precioCosto = _precioCosto;
    }
    public void setPrecioVenta(double _precioVenta) {
        this._precioVenta = _precioVenta;
    }
    
        //obtener el valor de la cantidad
    //GETTERS            
    
    
    public static int getID() {
        return id;
    }            
    public int getCodigo(){
        return _codigo;
    }    
    public String getSCodigo(){
        return "PROD"+_codigo;
    }    
    public int getCantidad() {
        return _cantidad;
    }        
    public String getNombre() {
        return _nombre;
    }
    public double getPrecioCosto() {
        return _precioCosto;
    }
    public double getPrecioVenta() {
        return _precioVenta;
    }
    //clonación
    public SolarisM clone(){        
        return new Producto(this._codigo,this._nombre,this._cantidad,this._precioCosto,this._precioVenta);
    }
    //filtro para búsqueda - filtros aceptados (nombre,código)
    public boolean filtro(String tag,String str1){
        String str2;        
        switch(tag){
            case "NOMBRE":
                str2=this._nombre.toUpperCase();
            break;
            default:
                str2=this.getSCodigo();
        }
                        
        return str2.compareTo(str1)==0;
    }
    //BASE DE DATOS
    //ejecución del queries
    public void query(String tipoConsulta){
        Vector aux=this.toVector();
        switch(tipoConsulta){
            case "INSERT":
               
                Market.addProducto(this);
                Dao.insert("IDPRODUCTOS,NOMBRE,CANTIDAD,PC,PV",aux ,"PRODUCTOS");
            break;
            case "UPDATE":
                Dao.update("IDPRODUCTOS,NOMBRE,CANTIDAD,PC,PV",aux,"PRODUCTOS","IDPRODUCTOS = "+aux.get(0) );
            break;
            case "DELETE":
                Market.removeProductoWhere(this.getSCodigo());
                Dao.delete("PRODUCTOS","IDPRODUCTOS = "+this._codigo);
            break;
        }
        
    }
    //typecast
    public Vector toVector(){ //usado particularmente para devolver columnas a una tabla
        Vector<String> vct=new Vector();
        
        vct.add(""+this._codigo);
        vct.add(this._nombre);
        vct.add(this._cantidad+"");
        vct.add(String.valueOf(this._precioCosto));
        vct.add(String.valueOf(this._precioVenta));        
        return vct;
        
    }
    public String toString(){
        return "-Producto- Codigo: "+this._codigo+" Nombre: "+this._nombre+" Cantidad: "+this._cantidad+" PC: "+this._precioCosto+" PV: "+this._precioVenta;
    }
    //ID 
    public static int sgteID(){       
        id++;
        return id;
    }   
    //generar ID seteando los valores que se ingresan y cogiendo el mayor de ellos
    public static void generarID(int id){
        if(id>Producto.id)
            Producto.id=id;    
    }
}
