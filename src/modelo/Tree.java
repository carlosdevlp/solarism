/*Entidad Tree, para la creación de árboles binarios*/
package modelo;
import java.util.*;
/**
 *
 * @author carlos
 */

public class Tree<T>{
       private class Node<T>{
           private int _ind;           
           private T _value;           
           private Node<T> _right,_left;
           
           public Node(int _ind,T _value){
               this._value=_value;
               this._ind=_ind;
               this._right=null;
               this._left=null;
           }
           //getters
           public int getInd(){
               return _ind;
           }

           public T getValue() {
                return _value;
           }

           public Node getRight() {
                return _right;
           }

           public Node getLeft() {
                return _left;
           }

           
           //agregar nodo
           public void addNode(Node _node){
               if(_node.getInd()>this._ind) //mayores a la derecha
                   this.addRight(_node);
               else //menores a la izquierda
                   this.addLeft(_node);
           }
           
            
           //agregar a la izq o der
           private void addRight(Node _right){
            this._right=_right;
           }
           private void addLeft(Node _left){
            this._left=_left;   
           }
           
           //convertir los valores a string
           public String toString(){               
               return "-Node- ID: "+this._ind+" Value: "+this._value;
           }
       
       }
    
        //izquieda: los menores, derecha: los mayores
       private Node<T> _root,_node,_act;//nodo raíz,nodo en uso,nodo actual
       private int _length;
       //constructores
            //constructor por defecto del árbol
            public Tree(){
                this._length=0;
                this._root=null;                   
            }
            //constructor con parámetros
            public Tree(int ind,T value){           
                this._node=this._root=new Node(ind,value);
                this._length=1;
            }
       //volver a la raiz
       public void reset(){
           this._node=this._root;
       }    
       //devolver tamaño del árbol
       public int size(){
           return this._length;
       }
       //eliminar todo
       public void clear(){
           this._root=this._node=null;
           this._length=0;
       }
       //añadir nodo
       public void addNode(int ind,T value){  
           if(this._root!=null){//si ya se ha asignado un nodo raiz
               
                    //al insertar el nodo siempre recorrerlo desde el nodo raiz            
                   Node<T> node=new Node(ind,value);
                   Node<T> aux=_root,branch=_root;//raíz, zona de la memoria a insertar el nodo           
                   while(branch!=null){
                        aux=branch;
                        if(node.getInd()>aux.getInd())//a la derecha
                            branch=aux.getRight();
                        else //a la izquerda
                            branch=aux.getLeft();
                   }
                   aux.addNode(node);                             
                   this._node=node;//tomar el lugar del nodo actual
                   
           }
           else{//si no se ha asignado un nodo raiz
               Node<T> node=new Node(ind,value);
               this._root=node;
            }
           this._length++;
       }
       
       //búsqueda por árbol binario
       public boolean exists(int ind){
           boolean existe=false;
           Node<T> aux=_root;//siempre se inicia la búsqueda desde el root                              
           while(aux!=null){
           
               if(aux.getInd()==ind){//si ya encontro al nodo, salir del bucle
                   existe=true;
                   break;
               }else if(ind>aux.getInd())//ir a la derecha               
                   aux=aux.getRight();
               else    //ir a la izquierda
                   aux=aux.getLeft(); 
           }                                                  
           return existe;
       }
       //obtener valor desde una posición dada
       public T getValueAtInd(int ind){
           Node<T> aux=_root;//siempre se inicia la búsqueda desde el root           
           boolean existe=false;
           while(aux!=null){
               if(aux.getInd()==ind){//si ya encontro al nodo, salir del bucle
                   existe=true;
                   break;
               }else if(ind>aux.getInd())//ir a la derecha
               
                   aux=aux.getRight();
               else    //ir a la izquierda
                   aux=aux.getLeft(); 
           }            
                      
            if(!existe)
                System.out.println("El nodo buscado no existe.");
            else
                return aux.getValue();
           return null;
       }

      //recorridos
       //INORDEN
            public void inOrden(){
                inOrden(this._root);
            }
            public void inOrden(Node _node){
                if(_node!=null){
                
                  inOrden(_node.getLeft());//izquierda 
                  System.out.println(_node);
                  inOrden(_node.getRight());//Derecha
                }
            }
            
       public String toString(){
           return this._node.toString();
       }
       
       public String allToString(){
           String str="";           
           return str;
       }
       
       
       
//----------------------función solo para SolarisM----------------------------------------
            public void recorrer(){
                this.recorrer(this._root);
            }
            public void recorrer(Node _node){
                if(_node!=null){
                    this.recorrer(_node.getLeft());
                        int ind=_node.getInd();    
                        switch(_node.getValue().toString()){
                            case "UPDATE"://editar elementos
                                Vector<String> aux;
                                
                                switch(Util.pointerGForm.getGCodigo()){
                                    case "USER":
                                        //Market.editUsuario(ind,Util.pointerGForm.getGElementos().get(ind).toVector());
                                        break;
                                    case "PROD":
                                        Market.editProducto(ind,Util.pointerGForm.getGElementos().get(ind).toVector());
                                        aux=Util.pointerGForm.getGElementos().get(ind).toVector();
                                        Dao.update("Codigo,Nombre,Cantidad,PC,PV",aux,"Productos","Codigo = "+aux.get(0) );
                                        break;
                                    case "DIST":
                                        //Market.editDistribuidor(ind,Util.pointerGForm.getGElementos().get(ind).toVector());
                                        break;
                                    case "CLIENT":
                                        Market.editCliente(ind,Util.pointerGForm.getGElementos().get(ind).toVector());
                                        aux=Util.pointerGForm.getGElementos().get(ind).toVector();
                                        Dao.update("Codigo,Nombre,DNI,Telefono",aux,"Clientes","Codigo = "+aux.get(0) );
                                        break;
                                }
                                
                                System.out.println("UPDATING...");
                                break;
                                
                                
                            case "INSERT"://agregar un nuevo elemento
                                switch(Util.pointerGForm.getGCodigo()){
                                    case "USER":
                                        Usuario nUsuario=new Usuario();
                                        nUsuario.set(Util.pointerGForm.getGElementos().get(ind).toVector());
                                        Market.addUsuario(nUsuario);
                                        break;
                                    case "PROD":
                                        Producto nProducto=new Producto();
                                        nProducto.set(Util.pointerGForm.getGElementos().get(ind).toVector());
                                        Market.addProducto(nProducto);
                                        Dao.insert("Codigo,Nombre,Cantidad,PC,PV",Util.pointerGForm.getGElementos().get(ind).toVector() , "Productos");
                                        break;
                                    case "DIST":
                                        Distribuidor nDistribuidor=new Distribuidor();
                                        Market.addDistribuidor(nDistribuidor);
                                        break;
                                    case "CLIENT":
                                        Cliente nCliente=new Cliente();                                        
                                        nCliente.set(Util.pointerGForm.getGElementos().get(ind).toVector());
                                        Market.addCliente(nCliente);
                                        Dao.insert("Codigo,Nombre,DNI,Telefono",Util.pointerGForm.getGElementos().get(ind).toVector() , "Clientes");
                                        break;
                                }
                                
                                System.out.println("INSERTING...");
                                break;
                                
                            case "DELETE":
                                switch(Util.pointerGForm.getGCodigo()){
                                    case "USER":
                                        
                                        break;
                                    case "PROD":                                                                                      
                                        Market.removeProductoWhere(Util.pointerGForm.getGElementos().get(ind).getSCodigo());
                                        Dao.delete("Productos","Codigo = "+Util.pointerGForm.getGElementos().get(ind).getCodigo());
                                        break;
                                    case "DIST":
                                        
                                        break;
                                    case "CLIENT":
                                        Market.removeClienteWhere(Util.pointerGForm.getGElementos().get(ind).getSCodigo());
                                        Dao.delete("Clientes","Codigo = "+Util.pointerGForm.getGElementos().get(ind).getCodigo());
                                        break;
                                }
                                
                                System.out.println("DELETING...");
                                break;
                        }                        
                    this.recorrer(_node.getRight());
                }
            }
            
       
       
}
