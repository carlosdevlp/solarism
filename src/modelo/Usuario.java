/*Entidad Abstracta Usuario */
package modelo;
import java.util.*;
/**
 *
 * @author carlos
 */
public class Usuario implements SolarisM{
    private static int id=0;
    private String _idUsuario,_username,_password,_tipo;
    private int _codigo;

    public Usuario(){
        //cuando se crea desde una fábrica
        this._idUsuario="USER"+Usuario.generador();
    }
    
    public Usuario(int _codigo,String _username, String _password) {
        this._codigo=_codigo;
        this._username = _username;
        this._password = _password;
    }
    
    public Usuario(String _username, String _password,String _tipo) {
        //this._idUsuario="USER"+Usuario.generador();       
        this._username = _username;
        this._password = _password;
        this._tipo=_tipo;
    }
    
    
    private static int generador(){       
        id++;
        return id;
    }    

    //Setters
        //super-set        
        public void set(Vector<String> str){
            this._codigo=Integer.parseInt(str.get(0));
            this._username=str.get(1);
            this._password=str.get(2);
        }
    public void setCodigo(int _codigo){
        this._codigo=_codigo;
    }
    public void setUsername(String _username) {
        this._username = _username;
    }
    public void setPassword(String _password) {
        this._password = _password;
    }
    public void setTipo(String _tipo) {
        this._tipo = _tipo;
    }
    
    //getters
    public int getCodigo(){
        return _codigo;
    }
    public String getSCodigo(){
        return "CLIENT"+this._codigo;
    }            
    public String getUsername() {
        return _username;
    }
    public String getTipo() {
        return _tipo;
    }
    public String getPassword() {
        return _password;
    }
    
    //clonación
    public SolarisM clone(){
           return new Usuario(this._codigo,this._username,this._password);
    }
    
    //filtro para búsqueda - filtros aceptados (nombre,código)
    public boolean filtro(String tag,String str1){
        
        String str2;        
        switch(tag){
            case "USUARIO":
                str2=this._username.toUpperCase();
            break;
            default:
                str2=this.getSCodigo();
        }                        
        return str2.compareTo(str1)==0;
        
    }
    
            
    //funcion para usuario y contraseña ingresados
    public boolean checkAccount(String username,String password){       
        if(this._username.compareTo(username)==0 && this._password.compareTo(password)==0) 
            return true;
        else 
            return false;
        
    }
    //BASE DE DATOS
    //ejecución del queries
    public void query(String tipoConsulta){
        Vector aux=this.toVector();
        switch(tipoConsulta){
            case "INSERT":                
                Dao.insert("IDEMPLEADO,USUARIO,CLAVE,TIPO",aux ,"USUARIO");
            break;
            case "UPDATE":                
                Dao.update("IDEMPLEADO,USUARIO,CLAVE,TIPO",aux ,"USUARIO","IDEMPLEADO = "+this._codigo);
            break;
            case "DELETE":
                Dao.delete("USUARIO","IDEMPLEADO = "+this._codigo);
            break;
        }
        
    }
    //typecast
    public Vector toVector(){
        Vector<String> vt=new Vector();
        vt.add(this._codigo+"");
        vt.add(this._username);
        vt.add(this._password);
        vt.add(this._tipo);
       return vt;
    }

}



/*
     Es recomendable tener la función mágica toString en clases como esta
        public String toString(){
        return this._username;
    }*/