/*Entidad gráfico */


package modelo;
import java.util.*;
import java.awt.Color;
import javax.swing.JPanel;
import org.jfree.chart.ChartFactory; 
import org.jfree.chart.ChartPanel; 
import org.jfree.chart.JFreeChart; 
import org.jfree.chart.plot.PlotOrientation; 
import org.jfree.data.category.DefaultCategoryDataset;


/**
 *
 * @author carlos
 */
public class Grafico {
    private Color _color;    
    private String _title,_ejeXNombre,_ejeYNombre,_variableNombre;
    private DefaultCategoryDataset _data= new DefaultCategoryDataset();
    //constructor
        public Grafico(){
            
        }
        public Grafico(String _title,String _variableNombre,String _ejeXNombre,String _ejeYNombre,int []rgb){
            this._title=_title;
            this._color=new Color(rgb[0],rgb[1],rgb[2]);//creando un nuevo color para el gráfico
            this._variableNombre=_variableNombre;
            this._ejeXNombre=_ejeXNombre;
            this._ejeYNombre=_ejeYNombre;                        
        }
        
        
    //setters        
        
        public void setData(ArrayList<String> a,ArrayList<Double>  b){            
            if(a.size()==b.size())
                for(int i=0;i<a.size();i++)
                    this._data.addValue(b.get(i),this._variableNombre, a.get(i));            
        }
        public void addData(String a,Double b){
            this._data.addValue(b,this._variableNombre, b);
        }
        public void setColor(Color _color) {
            this._color = _color;
        }
        public void setTitle(String _title) {
            this._title = _title;
        }
        public void setEjeXNombre(String _ejeXNombre) {
            this._ejeXNombre = _ejeXNombre;
        }
        public void setEjeYNombre(String _ejeYNombre) {
            this._ejeYNombre = _ejeYNombre;
        }
        

    //getters
        public Color getColor() { 
            return _color; 
        } 
        public String getTitle() {
            return _title;
        }
        public String getEjeXNombre() {
            return _ejeXNombre;
        }
        public String getEjeYNombre() {
            return _ejeYNombre;
        }

    //graficar
    public JFreeChart graficar() {        
        //------------------ Generando el gráfico--------------------------------
        
        JFreeChart chart = ChartFactory.createBarChart(
                this._title,//título del gráfico
                this._ejeXNombre,//nombre del eje X
                this._ejeYNombre,//nombre del eje Y
                this._data,//la data
                PlotOrientation.VERTICAL,//posición vertical
                true, true, false);
        
        
        //cambiar de color
        chart.getCategoryPlot().getRenderer().setSeriesPaint(0, this._color);
        return chart;
    }
       


    
    
    //obtener el gráfico como un panel
    public JPanel getGrafico(){
        return new ChartPanel(this.graficar());
    }
        
}





/**
 *
 * @author carlos
 */

        