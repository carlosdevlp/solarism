/*Entidad alternativa que tiene muchas funciones útiles para diferentes formularios */
package modelo;
import java.awt.Image;
import java.util.*;
import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;
import java.lang.reflect.Method;
import java.text.DecimalFormat;
import java.io.Serializable;
import javax.swing.filechooser.FileNameExtensionFilter;

/** 
 *
 * @author carlos
 */

abstract public class Util {
    //
    public static class TableModel{
        private DefaultTableModel _tbModel;

        public TableModel(DefaultTableModel _tbModel) {
            this._tbModel = _tbModel;
        }
        
        public TableModel(){            
        }

        public DefaultTableModel getTbModel() {
            return _tbModel;
        }

        public void setTbModel(DefaultTableModel _tbModel) {
            this._tbModel = _tbModel;
        }
        
        
    }
    //interface función, para crear contenido de funciones y pasarlas como parámetro
    public static interface Function{
        void execute();
    }
    public static class Handler implements ActionListener {
        
        private Function _callback;        
        public Handler(Function _callback){
          this._callback=_callback;          
        }        
        //cuando se ejecutó alguna acción
        public void actionPerformed(ActionEvent actionEvent) {
          //System.out.println("Selected: " + actionEvent.getActionCommand());
            this._callback.execute();
        }
        
    }
    
    public interface GeneralTable{
        public void setPointerGTableModel(TableModel _tbModel);
        public void setPointerGTable(JTable _tb);
        public void setGRow(int _row);
        public void setGCol(int _col);
        public void setPointerGTablaVacia(JLabel _lblTablaVacia);
        public void setGColumnsName(String[]  _columnsName);
        public int getGRow();
        public int getGCol();
        public String[] getGColumnsName();
        public TableModel getPointerGTableModel();
        public JTable getPointerGTable();        
        public JLabel getPointerGTablaVacia();
        public void addRow(Vector<String> args);
        public void editRow(int row,String []args);
        public void editRowItem(int row,int col,String str);
        public void setTable();
        public ArrayList<String> getRowItems(int row);
        public ArrayList<String> getColumnItems(int col);        
        public String getRowItem(int row,int col);
        public void removeRow(int row);
        public void reset();
        public void clear();
        
    }
    public interface GeneralForm{
        public void setGFrm(JFrame _frm);
        public void setGCodigo(String _codigo);
        public void setGElementoInd(int _elementoInd);
        public void setGTag(String _tag);
        public void setGElemento(SolarisM _elemento);
        public void setGElementos(SolarisM[] _elementos);
        public void setGControls(JComponent[] _controls);
        public void setGInitControls(String _initControls);
        public void setGContextMenu(JPopupMenu _contextMenu);        
        public void addGContextMenuItems(String []_items,Function []cbs);
        public void setGComponent(JTextField _component);
        public void setGValue(String _value);
        public JFrame getGFrm();
        public String getGCodigo();
        public ArrayList<SolarisM> getGElementos();
        public SolarisM getElementosWhere(String codigo);
        public String getGTag();
        public SolarisM getGElemento();        
        public int getGElementoInd();
        public JComponent[] getGControls();        
        public String getGInitControls(); 
        public JPopupMenu getGContextMenu();
        public JTextField getGComponent();
        public String getGValue();
        public void hideItem(int i);
        public void showItem(int i);
        public void hideContextMenu();
        public void showContextMenu();
        public void agregarElemento(SolarisM _elemento);
        public void editarElemento(int ind, Vector<String> str);
        public void eliminarElemento(int ind);
        public void hide();
        public void show();
        public void reset();
    }
    public interface Format{
        public String Decimal(double num);
        public String currentDate();
        public String formatDate(String fec,String sep);
        public String divide(String a,String b);
        public ArrayList<String> divide(ArrayList<String> a,String b);
    }
    public interface SSystem {
        public void init();
        public void setConfBDNombre(String _confBDNombre);
        public void setConfBDUsername(String _confBDUsername);
        public void setConfBDPassword(String _confBDPassword);
        public void setConfIPServ(String _confIPServ);
        public void setConfUsuario(Usuario _confUsuario);
        public void setConfEmpleado(Empleado _confEmpleado);
        public void setConfMasterUsername(int _confMasterUsername);
        public void setConfMasterPassword(int _confMasterPassword);
        public String getConfBDNombre();
        public String getConfBDUsername();
        public String getConfBDPassword();
        public String getConfIPServ();
        public int getConfMasterUsername();
        public int getConfMasterPassword();
        public Usuario getConfUsuario();
        public Empleado getConfEmpleado();
    }
    //objeto del sistema
    public static SSystem Ssystem=new SSystem(){
                              //configuración del sistema
                              private String _confBDNombre="SOLARISM";
                              private String _confBDUsername="local";
                              private String _confBDPassword="123456789123456789";
                              private String _confIPServ="localhost";
                              //datos del usuario
                              private Usuario _confUsuario;
                              private Empleado _confEmpleado;
                              //usuario maestro (ENCRIPTADO CON HASH)
                              private int _confMasterUsername;
                              private int _confMasterPassword;
                              
                              //init
                              public void init(){
                                  //asignación de datos maestros
                                  this._confMasterUsername="CCL".hashCode();
                                  this._confMasterPassword="SENIOR".hashCode();                                  
                                  ArrayList<String> aux;
                                  try{
                                    aux=Util.readFile("conf.s");
                                    this._confBDNombre=aux.get(0);                                    
                                    this._confBDUsername=aux.get(1);
                                    this._confBDPassword=aux.get(2);
                                    this._confIPServ=aux.get(3);
                                  }
                                  catch(Exception err){
                                      try{
                                        Util.writeFile("conf.s", new String[]{this._confBDNombre,this._confBDUsername,this._confBDPassword,this._confIPServ});
                                        JOptionPane.showMessageDialog(null, "Se ha creado un archivo de configuración del sistema.");
                                      }
                                      catch(Exception err2){
                                          System.out.println(err2);
                                      }                                      
                                  }
                                  
                                  
                              }
                              
                              //setters
                                public void setConfBDNombre(String _confBDNombre){
                                    this._confBDNombre=_confBDNombre;
                                }
                                public void setConfBDUsername(String _confBDUsername) {
                                    this._confBDUsername = _confBDUsername;
                                }
                                public void setConfBDPassword(String _confBDPassword) {
                                    this._confBDPassword = _confBDPassword;
                                }
                                public void setConfIPServ(String _confIPServ) {
                                    this._confIPServ = _confIPServ;
                                }
                                public void setConfUsuario(Usuario _confUsuario) {
                                    this._confUsuario = _confUsuario;
                                }
                                public void setConfEmpleado(Empleado _confEmpleado) {
                                    this._confEmpleado = _confEmpleado;
                                }
                                public void setConfMasterUsername(int _confMasterUsername) {
                                    this._confMasterUsername = _confMasterUsername;
                                }
                                public void setConfMasterPassword(int _confMasterPassword) {
                                    this._confMasterPassword = _confMasterPassword;
                                }
                                
                              //getters
                                public String getConfBDNombre(){
                                    return _confBDNombre;
                                }
                                public String getConfBDUsername() {
                                    return _confBDUsername;
                                }
                                public String getConfBDPassword() {
                                    return _confBDPassword;
                                }
                                public String getConfIPServ() {
                                    return _confIPServ;
                                }
                                public Usuario getConfUsuario() {
                                    return _confUsuario;
                                }
                                public Empleado getConfEmpleado() {
                                    return _confEmpleado;
                                }
                                public int getConfMasterUsername() {
                                    return _confMasterUsername;
                                }
                                public int getConfMasterPassword() {
                                    return _confMasterPassword;
                                }
                                
                                
                              
                        };
    //FORMAT
    //objeto para obtener formatos
    public static Format format=new Format(){
                                private DecimalFormat _fixed=new DecimalFormat("#0.00");
                                public String Decimal(double num){                                    
                                      //return _fixed.format(num);
                                    return String.format(Locale.ENGLISH, "%.2f", num);
                                }                               
                                //obtener fecha actual formateada
                                public String currentDate(){
                                    Date fechaActual=new Date();//obteniendo la fecha actual del sistema
                                    return  ((fechaActual.getDay()>9)?"":"0")+fechaActual.getDay()
                                            +"/"+((fechaActual.getMonth()>9)?"":"0")+fechaActual.getMonth()
                                            +"/"+(1900+fechaActual.getYear());
                                }
                                //formatear fechas
                                public String formatDate(String fec,String sep){
                                    String []a=fec.split(sep);
                                    String []b=a[2].split(" ");
                                    String c="";                                    
                                    a[2]=b[0];                                                                                                            
                                    for(int i=a.length-1;i>=0;i--)                                    
                                           c+=a[i]+((i!=0)?"/":"");
                                    b=null;
                                    a=null;                                    
                                    return c;
                                }
                               //desencadenar string
                                public String divide(String a,String b){
                                    String []c=a.split(b);  
                                    return c[1];
                                }
                               //desencadenar strings
                                public ArrayList<String> divide(ArrayList<String> a,String b){
                                    String []c;  
                                    ArrayList<String> e=new ArrayList();
                                    for(String d : a){
                                            c=d.split(b);
                                            e.add(c[1]);
                                            c=null;
                                    }
                                    return e;
                                } 
                          };
    //punteros globales de objetos más recurrentes en el formulario
    public static GeneralForm pointerGForm=new GeneralForm(){
                                            private JFrame _frm;
                                            private ArrayList<SolarisM> _elementos;
                                            private int _elementoInd;
                                            private SolarisM _elemento;//puntero a un elemento de elementos
                                            private String _codigo,_tag;
                                            private JComponent []_controls=new JComponent[]{};
                                            private JTextField _component;//puntero a un componente
                                            private String _value="";//un valor almacenado y a disposición de todo el programa
                                            private String _initControls="";//determinar como deben inicializarse los controles de un formulario
                                            private JPopupMenu _contextMenu=new JPopupMenu();//único menu para todos los controles que deseen usarlo
                                            private ArrayList<JMenuItem> _menuItems=new ArrayList();
                                            //setters
                                            public void setGFrm(JFrame _frm) {                                                
                                                this._frm = _frm;
                                                this._controls=null;//eliminar rastros pertenecientes a otros controles de otros formularios
                                                this._controls=new JComponent[]{};//para evitar errores colocar un conjunto vacío
                                            }       
                                            public void setGElementos(SolarisM[] _elementos) {
                                                this._elementos =new ArrayList(Arrays.asList(_elementos));
                                            }
                                            public void setGCodigo(String _codigo){
                                                this._codigo=_codigo;
                                            }       
                                            public void setGElementoInd(int _elementoInd) {
                                                this._elementoInd = _elementoInd;
                                            }
                                            public void setGTag(String _tag){
                                                this._tag=_tag.toUpperCase();
                                            }  
                                            public void setGElemento(SolarisM _elemento){
                                                this._elemento=_elemento;
                                            }        
                                            public void setGControls(JComponent[] _controls) {
                                                this._controls = _controls;
                                            }       
                                            public void setGInitControls(String _initControls) {
                                                this._initControls = _initControls;
                                            }
                                            public void setGContextMenu(JPopupMenu _contextMenu) {
                                                this._contextMenu = _contextMenu;                                                
                                            }
                                            public void addGContextMenuItems(String []_items,Function []cbs){
                                                //this._contextMenu.removeAll();                                               
                                                ActionListener actionListener;
                                                JMenuItem menuItem;
                                                if(_items.length==cbs.length)
                                                        for(int i=0;i<_items.length;i++){
                                                            //construyendo menu item
                                                            actionListener = new Handler(cbs[i]);
                                                            menuItem=new JMenuItem(_items[i]);                                                            
                                                            menuItem.addActionListener(actionListener);
                                                            //añadiendo el nuevo menu item                                                            
                                                            this._menuItems.add(menuItem);                                                            
                                                            this._contextMenu.add(menuItem);
                                                            actionListener =null;                                                            
                                                        }
                                                
                                            }
                                            public void setGComponent(JTextField _component) {
                                                this._component = _component;
                                            }
                                            public void setGValue(String _value) {
                                                this._value = _value;
                                            }
                                            
                                            //getters
                                            public JFrame getGFrm() {
                                                return _frm;
                                            }                                                 
                                            public String getGCodigo(){
                                                return _codigo;
                                            }                                            
                                            public ArrayList<SolarisM> getGElementos() {
                                                return _elementos;
                                            }
                                            public SolarisM getElementosWhere(String codigo){                                                 
                                                for(int i=0;i<_elementos.size();i++)
                                                    if(_elementos.get(i).getSCodigo().compareTo(codigo)==0){
                                                        this.setGElementoInd(i);
                                                        return _elementos.get(i);
                                                    }                                                
                                                return null;
                                            }                                            
                                            public int getGElementoInd() {
                                                 return _elementoInd;
                                            }
                                            public SolarisM getGElemento(){
                                                    return _elemento;
                                            }                                           
                                            public String getGTag(){
                                                return _tag;
                                            }
                                            public JComponent[] getGControls() {
                                                return _controls;
                                            }
                                            public String getGInitControls() {
                                                return _initControls;
                                            }
                                            public JPopupMenu getGContextMenu() {
                                                return _contextMenu;
                                            }
                                            public JTextField getGComponent() {
                                                return _component;
                                            }
                                            public String getGValue() {
                                                return _value;
                                            }
                                            //ContexMenu
                                            public void hideItem(int i){
                                                this._menuItems.get(i).hide();
                                            }
                                            public void showItem(int i){
                                                this._menuItems.get(i).show();
                                            }
                                            public void hideContextMenu(){
                                                this._contextMenu.hide();
                                            }
                                            public void showContextMenu(){
                                                this._contextMenu.show();
                                            }
                                                //agregar elemento                                            
                                                public void agregarElemento(SolarisM _elemento){
                                                    this._elementos.add(_elemento);
                                                    this._elemento=_elemento;                                                    
                                                }

                                                //editar elemento
                                                public void editarElemento(int ind, Vector<String> str){
                                                    this._elementos.get(ind).set(str);
                                                    this._elemento=this._elementos.get(ind);                                                 
                                                }
                                                //eliminar elemento
                                                public void eliminarElemento(int ind){                                                
                                                     this._elemento=this._elementos.get(ind);
                                                     this._elementos.remove(ind);                                                     
                                                }
                                            //ocultar formulario
                                            public void hide(){
                                                this._frm.hide();
                                            }
                                            //mostrar formulario
                                            public void show(){
                                                this._frm.show();
                                            }
                                            //resetear o reconfigurar todo con el formulario actual
                                            public void reset(){
                                                //reconfigurar los controles asignados del formulario actual
                                                switch(this._initControls){
                                                    case "LOCK":
                                                        Util.lock(_controls);                                                       
                                                        break;
                                                    case "HIDE":
                                                        Util.hide(_controls);
                                                        break;
                                                    default:
                                                        Util.unlock(_controls);
                                                        Util.show(_controls);
                                                }
                                                this._initControls="";
                                            }
                              };
    public static GeneralTable pointerGTable=new GeneralTable(){
                                                    private TableModel _tbModel=new TableModel();
                                                    private int _row,_col;
                                                    private JTable _tb;
                                                    private JLabel _lblTablaVacia;
                                                    private String []_columnsName;                                                    
                                                    //setters
                                                        //apuntar a otra tabla modelo
                                                        public void setPointerGTableModel(TableModel _tbModel){
                                                            this._tbModel=_tbModel;                                                        
                                                        }                                                        
                                                        public void setPointerGTable(JTable _tb){
                                                            this._tb=_tb;
                                                        }
                                                        public void setGRow(int _row){
                                                            this._row=_row;
                                                        }
                                                        public void setGCol(int _col) {
                                                            this._col = _col;
                                                        }
                                                        public void setPointerGTablaVacia(JLabel _lblTablaVacia){
                                                            this._lblTablaVacia=_lblTablaVacia;
                                                        }
                                                        public void setGColumnsName(String[]  _columnsName){
                                                            this._columnsName=_columnsName;
                                                        }                                                        
                                                    //getters
                                                        //devolver una referencia a la tabla que se está apuntando
                                                        public TableModel getPointerGTableModel(){
                                                            return this._tbModel;
                                                        }
                                                        public JTable getPointerGTable(){
                                                            return this._tb;
                                                        }
                                                        public int getGRow() {
                                                            return _row;
                                                        }
                                                        public int getGCol() {
                                                            return _col;
                                                        }
                                                        public JLabel getPointerGTablaVacia(){
                                                           return _lblTablaVacia;
                                                        }
                                                        public String[] getGColumnsName(){
                                                            return _columnsName;
                                                        }
                                                    //agregar nueva fila a la tabla
                                                    public void addRow(Vector<String> args){
                                                        args.set(0,Util.pointerGForm.getGCodigo()+args.get(0));
                                                        this._tbModel.getTbModel().addRow(args);
                                                        this._lblTablaVacia.setVisible(false);
                                                    }
                                                   //editar el contenido de una fila de la tabla  
                                                    public void editRow(int row,String []args){       
                                                           for(int col=0;col<this._tbModel.getTbModel().getColumnCount();col++)
                                                            this._tbModel.getTbModel().setValueAt(args[col], row, col);
                                                    }                                                   
                                                   //editar el contenido de un elemento en una fila 
                                                    public void editRowItem(int row,int col,String str){
                                                        this._tbModel.getTbModel().setValueAt(str, row, col);
                                                    }
                                                    //llenar la tabla desde cero
                                                    public void setTable(){
                                                        this.clear();
                                                        int tam=0;                                                                                                                                                                           
                                                        //System.out.println(this._tbModel.getRowCount());
                                                        for(SolarisM row: Util.pointerGForm.getGElementos())                                                            
                                                            this.addRow(row.toVector());
                                                        
                                                        tam=Util.pointerGForm.getGElementos().size();
                                                                                                                
                                                        this._lblTablaVacia.setVisible(!(tam>0));
                                                        
                                                        
                                                    }
                                                    
                                                     //recibir datos de una fila 
                                                    public ArrayList<String> getRowItems(int row){
                                                        ArrayList<String> arr=new ArrayList();                                                        
                                                        for(int col=0;col<this._tbModel.getTbModel().getColumnCount();col++)
                                                            arr.add(this._tbModel.getTbModel().getValueAt(row, col).toString());
                                                        return arr;                                                        
                                                    }     
                                                    //recibir los datos de todos los elementos en una columna determinada
                                                    public ArrayList<String> getColumnItems(int col){
                                                        ArrayList<String> items=new ArrayList();
                                                        
                                                        for(int i=0;i<this._tbModel.getTbModel().getRowCount();i++)
                                                                items.add(this._tbModel.getTbModel().getValueAt(i, col).toString());
                                                        
                                                        return items;
                                                    }
                                                    //recibir datos de una columna en una fila determinada
                                                    public String getRowItem(int row,int col){
                                                        if(this._tbModel.getTbModel().getRowCount()>0)
                                                            return this._tbModel.getTbModel().getValueAt(row, col).toString();
                                                        return "";
                                                    }
                                                    
                                                    //eliminar un contenido de la tabla
                                                    public void removeRow(int row){
                                                        this._tbModel.getTbModel().removeRow(row);
                                                        this._lblTablaVacia.setVisible(!(this._tbModel.getTbModel().getRowCount()>0));
                                                    }                                                    
                                                    public void reset(){
                                                        this._tbModel.setTbModel(null);//eliminar la referencia anterior para evitar STACKOVERFLOW a mayor escala
                                                        this._tbModel.setTbModel(new DefaultTableModel(null,this._columnsName));//referenciar a un nuevo modelo en el heap
                                                        this._tb.setModel(this._tbModel.getTbModel());//incorporar ese nuevo modelo en la tabla
                                                        
                                                    }
                                                    //eliminar todo el contenido de la tabla
                                                    public void clear(){                                                                                                                                                           
                                                            //crear nuevo modelo para la tabla                                                           
                                                            this.reset();                                                        
                                                        this._lblTablaVacia.setVisible(true);
                                                    }
                                                    
                                            };
    
    //Evaluar el contenido del formulario
    public static void chequear(String []arr,int [][]res) throws Exception{        
        boolean err;//variable que identifica el error        
        //algoritmo para evaluar contenido no válido del formulario
        //white space o campos vacíos -restricción 1-        
        for(String palabra: arr){
            err=true;
            for(char caracter: palabra.toCharArray())
                if(caracter!=' '){
                    err=false;
                    break;                    
                }
            
            if(err)
               throw new Exception("Partes del formulario están vacías");
        }
        //campos numéricos no válidos -restricción 2-       
        try{
           if(res!=null)
            for(int i=res[0][0];i<res[0][1];i++)                
                Double.parseDouble(arr[i]);
            
        }catch(Exception e){
            throw new Exception("En algunas partes del formulario, se esperaba el ingreso de números.");
        }
    }
    
    /*funcion solo util cuando vas a modificar un elemento, porque se necesita saber 
    si los datos del elemento han variado*/
    public static boolean aModificar(Vector<String>Antes, Vector<String>Despues){
        //System.out.println("A: "+Antes.size()+" B: "+Despues.size());
        for(int i=1;i<Antes.size();i++)
            if(Antes.get(i).compareTo(Despues.get(i))!=0)
                return true;
        return false;       
        
    }
    
    //eleminar el contenido de los strings
    public static void vaciar(JTextField[] strs){
        for(JTextField str:strs) 
            str.setText("");        
    }
    
    //rellenar datos para modificarlos en un formulario de edición
    public static void rellenar(Vector<String> Antes,JTextField []tbxs){
        tbxs[0].setText(Util.pointerGForm.getGCodigo()+Antes.get(0));
        for(int i=1;i<tbxs.length;i++)
            tbxs[i].setText(Antes.get(i));
    }
    
    //plantilla de acciones cuando se manda a modificar un dato
    public static void modificar(int row,JFrame frm){
        if(row!=-1){    //-1 = fila no seleccionada            
            Util.pointerGTable.setGRow(row);            
            frm.show();
        }else
            JOptionPane.showMessageDialog(null,"Debe seleccionar una fila para editar");
        
    }
    //plantilla de acciones cuando se va a eliminar
    public static void eliminar(int row){  
        if(row!=-1){
            
            if(Util.aGuardar()){
                
                String codigo=Util.pointerGTable.getRowItem(row, 0);//obtener la columna código de la fila seleccionada
                Util.pointerGForm.getElementosWhere(codigo);//mover el indice del elemento al que tiene el mismo código que el que está en la tabla que voy a eliminar
                Util.pointerGForm.eliminarElemento(Util.pointerGForm.getGElementoInd());
                Util.pointerGTable.removeRow(row);
                Util.guardar("DELETE");
                
            }
        }
        else
            JOptionPane.showMessageDialog(null,"Debe seleccionar una fila para editar");
    }
    /*
        Funciones para bloquear o desbloquar un conjunto de funciones
        tiene una condición para evitar desbloquar controles previamente desbloqueados.
    */
    //función para bloquear una serie de controles
    public static void lock(JComponent []arr){
        if(arr.length>0)
            for(JComponent e:arr)
                if(e.isEnabled())
                    e.setEnabled(false);
                else
                    break;
        
    }
    //función para desbloquear una serie de controles
    public static void unlock(JComponent []arr){
        if(arr.length>0)
            for(JComponent e:arr)
                if(!e.isEnabled())
                    e.setEnabled(true);
                else
                    break;
        
    }
    //función para esconder una serie de controles
    public static void hide(JComponent []arr){
        if(arr.length>0)
            for(JComponent e:arr)
                if(e.isShowing())
                    e.hide();
                else
                    break;        
    }
    //función para mostrar una serie de controles
    public static void show(JComponent []arr){
        if(arr.length>0)
            for(JComponent e:arr)
                if(!e.isShowing())
                    e.show();
                else
                    break;        
    }
    //función para setear texto a una serie de controles
    public static void setText(JTextField e, String str){
        e.setText(str);
    }
    public static void setTexts(JTextField []arr, String []str){
        if(arr.length>0 && str.length>0 && arr.length==str.length)
                for(int i=0;i<arr.length;i++)
                    arr[i].setText(str[i]);
    }    
    
    //función para obtener todos los textos de una serie de controles
    public static String getText(JTextField e){
        return e.getText();
    }
    public static ArrayList<String> getTexts(JTextField []arr){
           ArrayList<String> str=new ArrayList();
                for(JTextField e:arr)
                      str.add(e.getText());
          return str;
    }
    
    
    
    //devuelve la confirmación del usuario para actualizar la data
    public static boolean aGuardar(){        
        
        if(JOptionPane.showConfirmDialog(null, "¿Desea realmente actualizar los datos?", "Confirmar", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE)!=0){
               JOptionPane.showMessageDialog(null, "No se actualizaron los datos.");//no
               return false;    
        }
        return true;//si
                
    }
    //guardar los valores directamente en la base de datos
    public static void guardar(String tipoConsulta){
        Util.pointerGForm.getGElemento().query(tipoConsulta);        
    }   
    
    //plantilla de acciones cuando se resetea el formulario
    public static void reset(JLabel lblTablaVacia,SolarisM []arr){
            //reconfigurar todo el formulario actual
            Util.pointerGForm.reset();
            //setear la tabla
            Util.pointerGForm.setGElementos(arr);
            Util.pointerGTable.setPointerGTablaVacia(lblTablaVacia);
            Util.pointerGTable.setTable();
            
    }
    public static void reset(String codigo,JTable tb,TableModel tbModel,String [] columns,JLabel lblTablaVacia,SolarisM [] arr){
            //reconfigurar todo el formulario actual
            Util.pointerGForm.reset();
            //setear la tabla
            Util.pointerGForm.setGCodigo(codigo);
            Util.pointerGTable.setPointerGTable(tb);
            Util.pointerGTable.setPointerGTableModel(tbModel);
            Util.pointerGTable.setPointerGTablaVacia(lblTablaVacia);
            Util.pointerGTable.setGColumnsName(columns);
            Util.pointerGForm.setGElementos(arr);
            Util.pointerGTable.setTable();
            
    }
     public static void reset(String _codigo,JTable tbVenta,TableModel tbModel,String[] _columnsName,JLabel lblTablaVacia,boolean setear){  
            //setear la tabla            
            Util.pointerGForm.setGCodigo(_codigo); //código de un formulario
            Util.pointerGTable.setGColumnsName(_columnsName);//nombres de las columnas
            Util.pointerGTable.setPointerGTable(tbVenta);//referencia a una tabla        
            Util.pointerGTable.setPointerGTableModel(tbModel);//referencia a una tabla        
            Util.pointerGTable.setPointerGTablaVacia(lblTablaVacia);//label de la tabla para indicar que está vacía
            if(setear)
                Util.pointerGTable.setTable();//setear la tabla
            else
                Util.pointerGTable.clear();//crear un nuevo modelo
                    
            
    }
    //apuntar
     public static void point(String codigo,JTable tb,TableModel tbModel,String [] columns,JLabel lblTablaVacia,SolarisM [] arr){
            //forzar a todos los punteros a apuntar a mis elementos
            Util.pointerGForm.setGCodigo(codigo);
            Util.pointerGTable.setPointerGTable(tb);
            Util.pointerGTable.setPointerGTableModel(tbModel);
            Util.pointerGTable.setPointerGTablaVacia(lblTablaVacia);
            Util.pointerGTable.setGColumnsName(columns);
            Util.pointerGForm.setGElementos(arr);
            
     }
    //Escritura y lectura de archivos
     public static ArrayList<String> readFile(String file)throws Exception{
            ArrayList<String> r=new ArrayList(); 
            try{
               //mandar el archivo a leer
               File N=new File(file);
               FileInputStream N1= new FileInputStream(N);
               ObjectInputStream N2= new ObjectInputStream(N1);

               //leer línea  por línea
               while (true) 
                   r.add((String)(N2.readObject()));  


           } catch(Exception err){
               throw new Exception("Archivo no existe");  
           }   
           finally{
               return r;
           }
    }
    public static void writeFile(String file,String [] arr)throws Exception{
        try{
              //abrir o crear un archivo
              File N=new File(file);
              FileOutputStream N1= new FileOutputStream(N);
              ObjectOutputStream N2= new ObjectOutputStream(N1);
              //escribir en un archivo
              for(String e:arr)
                  N2.writeObject((String)e);

              N2.close();
        }catch(Exception err){
            throw new Exception(err);
        }
    }
    // File Picker
    public static String FilePicker(String name,String[] frmt,boolean Default){
        JFileChooser fpckr = new JFileChooser();
        FileNameExtensionFilter filter = new FileNameExtensionFilter(name,frmt);
        if(!Default)
            fpckr.removeChoosableFileFilter(fpckr.getFileFilter() );
        fpckr.addChoosableFileFilter(filter);                            
        fpckr.setCurrentDirectory(new File(System.getProperty("user.home")));
        
        int result = fpckr.showOpenDialog(Util.pointerGForm.getGFrm());
        if (result == JFileChooser.APPROVE_OPTION) {
            File file = fpckr.getSelectedFile();
            //System.out.println("Selected file: " + file.getAbsolutePath());  
            return file.getAbsolutePath();
        }
      return null;
    
    }
    //crear imagen a partir de url
    public static ImageIcon PickImage(String url,int width,int height){
        ImageIcon icon=new ImageIcon(url),newIcon;
        newIcon = new ImageIcon(icon.getImage().getScaledInstance(width, height,  java.awt.Image.SCALE_SMOOTH));
        
        return newIcon;
        
    }
    //clonación    
    public static  SolarisM []clonar(SolarisM[] a){
            SolarisM []b=new SolarisM[a.length];
            for(int i=0;i<a.length;i++)            
                b[i]=a[i].clone();            
            return b;
    }
    
    //calcular ganancia
    public static Double calcGanancia(String str1,String str2) {
        Double a,b;
        //parseo para A
        try{
            a=Double.parseDouble(str1)+0.0;                        
        }catch(Exception err){
            a=0.0;
        }        
        //parseo para B
        try{
            b=Double.parseDouble(str2)+0.0;   
        }catch(Exception err){
            b=0.0;
        }        
        
        return a-b;    
    }
    
    //Algoritmo de búsqueda
    public static void buscar(String str1,String tag){
        //algoritmo de búsqueda 2 (a nivel de pilas y tags)
        //System.out.println(Util.pointerGTable.getPointerGTableModel());
        Util.pointerGForm.setGTag(tag);   
        Util.pointerGTable.clear();                
        for(SolarisM e: Util.pointerGForm.getGElementos())//1er filtro
            if(e.filtro(Util.pointerGForm.getGTag(), str1) || str1.compareTo("")==0)//córto circuito favorable a una búsqueda específica
                            Util.pointerGTable.addRow(e.toVector());
        
    }
    
    
}
/*
    SolarisM 1.2
    Modificaciones:
    -función guardar obsoleto
    -árbol de actualizaciones obsoleto
    -copia de elementos obsoletos(el objeto es necesario pero, la clonación  no)
    -función del árbol recorrer obsoleto
*/
