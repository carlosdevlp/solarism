/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vista;
import modelo.*;
import java.util.*;
import javax.swing.*;
import javax.swing.text.JTextComponent;

/**
 *
 * @author carlos
 */
public class frmVerOperaciones extends javax.swing.JFrame {
    private ArrayList< ArrayList<String> > _cmbOptions=new ArrayList();
    private JComboBox []_cmb;
    private JTextComponent _txtbuscar;
    private String []_codigos=new String[]{"VENT","COMP"};
    private String [][]_columns=new String[][]{{"N°Venta","IDCliente","IDProductos","Cantidad","Fecha"},{"N°Compra","IDProveedor","IDProductos","Cantidad","Fecha"}};
    private Util.TableModel []_tableModels=new Util.TableModel[]{new Util.TableModel(),new Util.TableModel()};
    private SolarisM [][]_elements=new SolarisM[2][];
    private String _txtfiltro="NVenta";
    private frmGrafico _chart=new frmGrafico();
    
    //mis funciones
    private void init(){
        //nombre de las columnas del combo de Ventas
        this._cmbOptions.add(new ArrayList(Arrays.asList("NVenta","IDCliente","IDProducto","Fecha")));        
        //nombre de las columnas del combo de Compra
        this._cmbOptions.add(new ArrayList(Arrays.asList("NCompra","IDProveedor","Nombre","Fecha")));        
        this._cmb=new JComboBox []{this.cmbFiltroVentas,this.cmbFiltroCompras};        
        
        //crear dos combos
        for(int i=0;i<this._cmb.length;i++){
            this._cmb[i].removeAllItems();                                  
            for(int ii=0;ii<this._cmbOptions.get(i).size();ii++)
                    this._cmb[i].addItem(this._cmbOptions.get(i).get(ii));                            
        }
        this._cmb[1].hide();
        this._cmb[0].show();
        
        
    }
    private void swapTextBox(int i){
        this._txtfiltro=this._cmb[i].getSelectedItem().toString();
        if(this._cmb[i].getSelectedItem().toString().compareTo("Fecha")==0){
                 this.txtBuscar.setText("");
                 this.txtBuscar.hide();
                 this.txtBuscarFecha.show();
                 this._txtbuscar=this.txtBuscarFecha;
            }
            else{
                 this.txtBuscarFecha.setText("");
                 this.txtBuscar.show();
                 this.txtBuscarFecha.hide();
                 this._txtbuscar=this.txtBuscar;
            }
    }
    /**
     * Creates new form frmVerOperaciones
     */
    public frmVerOperaciones() {        
        initComponents();
        this.setLocationRelativeTo(null);
        init();
    }

    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        tbOperaciones = new javax.swing.JTabbedPane();
        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        lblTablaVaciaVentas = new javax.swing.JLabel();
        jScrollPane3 = new javax.swing.JScrollPane();
        tbVentas = new javax.swing.JTable();
        jPanel3 = new javax.swing.JPanel();
        jPanel8 = new javax.swing.JPanel();
        lblTablaVaciaCompras = new javax.swing.JLabel();
        jScrollPane4 = new javax.swing.JScrollPane();
        tbCompras = new javax.swing.JTable();
        btnBuscar = new javax.swing.JButton();
        cmbFiltroVentas = new javax.swing.JComboBox();
        cmbFiltroCompras = new javax.swing.JComboBox();
        txtBuscarFecha = new javax.swing.JFormattedTextField();
        txtBuscar = new javax.swing.JTextField();
        btnGrafico = new javax.swing.JButton();

        setTitle("Ver Operaciones");
        setResizable(false);
        addComponentListener(new java.awt.event.ComponentAdapter() {
            public void componentShown(java.awt.event.ComponentEvent evt) {
                formComponentShown(evt);
            }
        });
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        tbOperaciones.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                tbOperacionesStateChanged(evt);
            }
        });

        jPanel2.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        jPanel2.setFocusCycleRoot(true);

        lblTablaVaciaVentas.setText("No hay ninguna venta en la lista");

        tbVentas.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "N°Venta", "IDClientes", "IDProductos", "Cantidad", "Fecha"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.String.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }
        });
        jScrollPane3.setViewportView(tbVentas);

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(195, 195, 195)
                .addComponent(lblTablaVaciaVentas)
                .addContainerGap(198, Short.MAX_VALUE))
            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(jScrollPane3, javax.swing.GroupLayout.DEFAULT_SIZE, 589, Short.MAX_VALUE)))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(148, 148, 148)
                .addComponent(lblTablaVaciaVentas)
                .addContainerGap(160, Short.MAX_VALUE))
            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                    .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 311, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
        );

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 616, Short.MAX_VALUE)
            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel1Layout.createSequentialGroup()
                    .addGap(2, 2, 2)
                    .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(19, Short.MAX_VALUE)))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 338, Short.MAX_VALUE)
            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel1Layout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(9, Short.MAX_VALUE)))
        );

        tbOperaciones.addTab("Ventas", jPanel1);

        jPanel8.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        jPanel8.setFocusCycleRoot(true);

        lblTablaVaciaCompras.setText("No hay ninguna compra en la lista");

        tbCompras.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "N°Compra", "IDProveedor", "IDProductos", "Cantidad", "Fecha"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.String.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }
        });
        jScrollPane4.setViewportView(tbCompras);

        javax.swing.GroupLayout jPanel8Layout = new javax.swing.GroupLayout(jPanel8);
        jPanel8.setLayout(jPanel8Layout);
        jPanel8Layout.setHorizontalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel8Layout.createSequentialGroup()
                .addGap(195, 195, 195)
                .addComponent(lblTablaVaciaCompras)
                .addContainerGap(188, Short.MAX_VALUE))
            .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel8Layout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(jScrollPane4, javax.swing.GroupLayout.DEFAULT_SIZE, 591, Short.MAX_VALUE)))
        );
        jPanel8Layout.setVerticalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel8Layout.createSequentialGroup()
                .addGap(148, 148, 148)
                .addComponent(lblTablaVaciaCompras)
                .addContainerGap(159, Short.MAX_VALUE))
            .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel8Layout.createSequentialGroup()
                    .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, 310, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
        );

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 616, Short.MAX_VALUE)
            .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel3Layout.createSequentialGroup()
                    .addGap(2, 2, 2)
                    .addComponent(jPanel8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(17, Short.MAX_VALUE)))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 338, Short.MAX_VALUE)
            .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel3Layout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(jPanel8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(10, Short.MAX_VALUE)))
        );

        tbOperaciones.addTab("Compras", jPanel3);

        getContentPane().add(tbOperaciones, new org.netbeans.lib.awtextra.AbsoluteConstraints(6, 56, -1, 367));

        btnBuscar.setText("Buscar");
        btnBuscar.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnBuscarMouseClicked(evt);
            }
        });
        getContentPane().add(btnBuscar, new org.netbeans.lib.awtextra.AbsoluteConstraints(525, 6, -1, 38));

        cmbFiltroVentas.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "NVenta", "IDCliente", "IDProducto", "Fecha" }));
        cmbFiltroVentas.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cmbFiltroVentasItemStateChanged(evt);
            }
        });
        getContentPane().add(cmbFiltroVentas, new org.netbeans.lib.awtextra.AbsoluteConstraints(380, 10, 123, -1));

        cmbFiltroCompras.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "NVenta", "IDCliente", "IDProducto", "Fecha" }));
        cmbFiltroCompras.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cmbFiltroComprasItemStateChanged(evt);
            }
        });
        getContentPane().add(cmbFiltroCompras, new org.netbeans.lib.awtextra.AbsoluteConstraints(380, 10, 123, -1));

        try {
            txtBuscarFecha.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("##/##/####")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
        getContentPane().add(txtBuscarFecha, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 10, 360, -1));
        getContentPane().add(txtBuscar, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 10, 360, -1));

        btnGrafico.setText("Ver Gráfico");
        btnGrafico.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnGraficoMouseClicked(evt);
            }
        });
        getContentPane().add(btnGrafico, new org.netbeans.lib.awtextra.AbsoluteConstraints(380, 40, 120, 30));

        pack();
    }// </editor-fold>//GEN-END:initComponents

   //cuando el valor del filtro de Ventas cambio
    private void cmbFiltroVentasItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cmbFiltroVentasItemStateChanged
        try{
        this.swapTextBox(0);
        }catch(Exception err){}
    }//GEN-LAST:event_cmbFiltroVentasItemStateChanged
    //cuando el valor del filtro de Compras cambio
    private void cmbFiltroComprasItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cmbFiltroComprasItemStateChanged
        try{
        this.swapTextBox(1);
        }catch(Exception err){}
    }//GEN-LAST:event_cmbFiltroComprasItemStateChanged
    //CADA VEZ QUE APARECE EL FORMULARIO
    private void formComponentShown(java.awt.event.ComponentEvent evt) {//GEN-FIRST:event_formComponentShown
        Util.pointerGForm.setGFrm(this);
        this._elements[1]=Market.getCompras().toArray(new Compra[Market.getCompras().size()]);
        this._elements[0]=Market.getVentas().toArray(new Venta[Market.getVentas().size()]);       
        
        Util.reset(_codigos[1], tbCompras, _tableModels[1],_columns[1], lblTablaVaciaCompras,this._elements[1]);
        Util.reset(_codigos[0], tbVentas, _tableModels[0],_columns[0], lblTablaVaciaVentas,this._elements[0]);
        if(this._elements[0].length<=0)
            this.btnGrafico.hide();
        else
            this.btnGrafico.show();
        //Util.reset(this.lblTablaVaciaVentas,Market.getClientes().toArray(new Cliente[Market.getClientes().size()]));
    }//GEN-LAST:event_formComponentShown
    //cuando cambio de panel en el tabhost
    private void tbOperacionesStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_tbOperacionesStateChanged
            int ind=this.tbOperaciones.getSelectedIndex();
            try{
                this._cmb[1-ind].hide();
                this._cmb[ind].show();
                switch(ind){                                            
                    case 0://Ventas
                        Util.point(_codigos[0], tbVentas, _tableModels[0],_columns[0], lblTablaVaciaVentas,this._elements[0]);
                        this._txtfiltro=this.cmbFiltroVentas.getSelectedItem().toString();
                    break;
                    case 1://Compras
                        Util.point(_codigos[1], tbCompras, _tableModels[1],_columns[1], lblTablaVaciaCompras,this._elements[1]);
                        this._txtfiltro=this.cmbFiltroCompras.getSelectedItem().toString();
                    break;
                }
                
            }catch(Exception err){}
    }//GEN-LAST:event_tbOperacionesStateChanged
    //al buscar algún tipo de dato
    private void btnBuscarMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnBuscarMouseClicked
        Util.buscar(this._txtbuscar.getText().toUpperCase(), this._txtfiltro.toUpperCase() );
    }//GEN-LAST:event_btnBuscarMouseClicked

    private void btnGraficoMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnGraficoMouseClicked
        this._chart.show();
    }//GEN-LAST:event_btnGraficoMouseClicked

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(frmVerOperaciones.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(frmVerOperaciones.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(frmVerOperaciones.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(frmVerOperaciones.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new frmVerOperaciones().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnBuscar;
    private javax.swing.JButton btnGrafico;
    private javax.swing.JComboBox cmbFiltroCompras;
    private javax.swing.JComboBox cmbFiltroVentas;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JLabel lblTablaVaciaCompras;
    private javax.swing.JLabel lblTablaVaciaVentas;
    private javax.swing.JTable tbCompras;
    private javax.swing.JTabbedPane tbOperaciones;
    private javax.swing.JTable tbVentas;
    private javax.swing.JTextField txtBuscar;
    private javax.swing.JFormattedTextField txtBuscarFecha;
    // End of variables declaration//GEN-END:variables
}
