/*Formulario Principal SOLARIS */
package vista;

/**
 *
 * @author carlos
 */
import java.awt.Frame;
import modelo.*;
import java.util.*;
import java.util.Timer;
import javax.swing.JComponent;
import javax.swing.JOptionPane;


public class frmSolaris extends javax.swing.JFrame {
    //----------mis variables---------------    
    public static ArrayList<javax.swing.JFrame> frms=new ArrayList(); //todos los formularios que se podrian abrir en el sistema
    private static boolean _ingresar=false;
    private static JComponent []_menus;
    //----------mis funciones---------------
    private void init(){
        //iniciar el objeto del sistema
        Util.Ssystem.init();
        //crear una lista de menus
        _menus=new JComponent[]{this.mUsuario,this.mEmpleados,this.mProductos,this.mDistribuidores,this.mClientes,this.mOperaciones,this.mOpciones};
        //Obtener datos de la base de datos
            //usuarios                                                
            Dao.init(Util.Ssystem.getConfBDNombre());//inicializar la conexión con la base de datos
            
            ArrayList<ArrayList<String>> A;            
                        // columnas  /  tabla(s)  / condición / llaves foráneas para crear condición
            //A=Dao.select("USUARIO,CLAVE,TIPO", "USUARIO",null);
            A=Dao.select("EMPLEADO.IDEMPLEADO,NOMBRE,CARGO,DNI,DIRECCION,TELEFONO,USUARIO,CLAVE,TIPO",new String[]{"EMPLEADO","PERSONA","USUARIO"},new String[]{"EMPLEADO.IDPERSONA=PERSONA.IDPERSONA","USUARIO.IDEMPLEADO=EMPLEADO.IDEMPLEADO"},null);
            Market.createEmpleados(A);
            
        //Crear Formularios
                
                frms.add(new frmLogin());
                frms.get(0).show();frms.get(0).requestFocusInWindow();
                frms.add(new frmListaProductos());
                frms.add(new frmListaDistribuidores());
                frms.add(new frmListaClientes());
                frms.add(new frmVerOperaciones());
                frms.add(new frmNuevaVenta());
                frms.add(new frmConfiguracion());
                frms.add(new frmDatosPersonales());
                frms.add(new frmListaEmpleados());
        //objeto timer para evaluación constante de cambios en el sistema
        TimerTask task=new TimerTask(){
                        public void run(){
                            if(frmSolaris._ingresar)
                                frmSolaris.this.enable(true);
                            else
                                frmSolaris.this.enable(false);
                        }
                    };
        Timer timer=new Timer();
        timer.scheduleAtFixedRate(task, 0, 100);
        //estructurar nuestro context menu general
        Util.pointerGForm.addGContextMenuItems(new String[]{"Pasar Código","Eliminar","Eliminar Todo"},
                                               new Util.Function[]{
                                                   new Util.Function(){//pasar el código a un textBox
                                                       public void execute(){
                                                              int row=Util.pointerGTable.getPointerGTable().getSelectedRow();
                                                              if(row!=-1)
                                                                Util.setText(Util.pointerGForm.getGComponent(), Util.pointerGTable.getRowItem(row, 0));
                                                              else                                                                                                                            
                                                                JOptionPane.showMessageDialog(null, "Debe seleccionar una fila", "¡Alerta!",  JOptionPane.ERROR_MESSAGE);

                                                       }  
                                                    },
                                                    new Util.Function(){//eliminar una fila de la tabla
                                                       public void execute(){
                                                            
                                                              int row=Util.pointerGTable.getPointerGTable().getSelectedRow();
                                                              if(row!=-1){
                                                                  Double b=Double.parseDouble(Util.pointerGTable.getRowItem(row, 4)),
                                                                         a=Double.parseDouble(Util.getText(Util.pointerGForm.getGComponent()));
                                                                Util.setText(Util.pointerGForm.getGComponent(),Util.format.Decimal(a-b));
                                                                Util.pointerGTable.removeRow(row);
                                                                
                                                              }else                                                                                                                         
                                                                JOptionPane.showMessageDialog(null, "Debe seleccionar una fila para eliminar", "¡Alerta!",  JOptionPane.ERROR_MESSAGE);
                                                            
                                                         
                                                       }
                                                    },
                                                    new Util.Function(){//limpiar toda la tabla
                                                       public void execute(){                                                         
                                                         
                                                            Util.pointerGTable.clear();//limpiar todos los datos de la tabla
                                                            Util.setText(Util.pointerGForm.getGComponent(),"0.0");
                                                            JOptionPane.showMessageDialog(null,"Se eliminaron todas las filas de la tabla");
                                                            

                                                       }
                                                    }
                                               });
        System.out.println("listo..");
       //detalle para renderización de la ventana
        //this.setExtendedState(this.MAXIMIZED_BOTH);//ventana fullScreen
    }    
    
    //todos los datos del sistema y del usuario
    public static void setSolarisMData(){
            //obtener datos personales del usuario
           ArrayList<ArrayList<String>> A;           
           //configurar programa como usuario normal
           switch(Util.Ssystem.getConfEmpleado().getUsuario().getTipo()){
               case "NORMAL":
                Util.unlock(new JComponent[]{_menus[0],_menus[2],_menus[3],_menus[4],_menus[5]});
                A=Dao.select("VENTAS.IDVENTAS,IDEMPLEADO,IDCLIENTES,IDPRODUCTOS,CANTIDAD,FECHA", "VENTAS,PRODUCTOS_VENTAS", "IDVENTAS,IDVENTAS","IDEMPLEADO ="+Util.Ssystem.getConfEmpleado().getCodigo());
                Market.createVentas(A);               
               break;
               case "ADMIN":
                Util.unlock(new JComponent[]{_menus[0],_menus[1],_menus[2],_menus[3],_menus[4],_menus[5]});
                A=Dao.select("VENTAS.IDVENTAS,IDEMPLEADO,IDCLIENTES,IDPRODUCTOS,CANTIDAD,FECHA", "VENTAS,PRODUCTOS_VENTAS", "IDVENTAS,IDVENTAS",null);
                Market.createVentas(A);
               break;   
           }
            A=Dao.select("IDPRODUCTOS,NOMBRE,CANTIDAD,PC,PV","PRODUCTOS",null);
                Market.createProductos(A);
            A=Dao.select("IDDISTRIBUIDORES,NOMBRE,RUC", "DISTRIBUIDORES", null);
                Market.createDistribuidores(A);            
            A=Dao.select("IDCLIENTES,NOMBRE,DNI,TELEFONO", "CLIENTES,PERSONA", "IDPERSONA,IDPERSONA", null);
                Market.createClientes(A);
            
            A=Dao.select("PEDIDO.IDPEDIDO,IDEMPLEADO,IDDISTRIBUIDORES,IDPRODUCTOS,CANTIDAD,FECHA", "PEDIDO,PEDIDO_PRODUCTOS","IDPEDIDO,IDPEDIDO", null);
                Market.createCompras(A);
           
           
    }
    /*escondar todo el menu*/
    public static void hideMenu(){
        //ocultar todo el menu
            Util.hide(_menus);
    }
    /*Entrar al programa*/
    public static void ingresar(boolean entrar,boolean master){
        frmSolaris._ingresar=(entrar || master);
        Util.lock(_menus);
        //for(int i=0;i<7;i++)
          //  _menus[i].hide();
              
        //ocultar el login
        if(entrar){//si entra como usuario normal o administrador
           
           setSolarisMData();           
           frmSolaris.frms.get(0).hide();
           
           
        }else if(master){//si entra como master
            //configurar sistema como Master                
           Util.unlock(new JComponent[]{_menus[6]});           
           frmSolaris.frms.get(0).hide(); 
           JOptionPane.showMessageDialog(null,"Usted está entrando como Master.\ntiene el permiso para alterar el sistema.","¡Atención!",JOptionPane.WARNING_MESSAGE);
           
        }
        else
            JOptionPane.showMessageDialog(null, "Datos ingresados incorrectos.");
    }
    public static void setIngresar(boolean ingresar){
        frmSolaris._ingresar=ingresar;
    }
    
    
    /**
     * Creates new form frmSolaris
     */
    public frmSolaris() {
        initComponents();
        frmSolaris.this.setLocationRelativeTo(null);
        frmSolaris.this.init();
    
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        mdiSolaris = new javax.swing.JDesktopPane();
        menu = new javax.swing.JMenuBar();
        mUsuario = new javax.swing.JMenu();
        mUsuarioDatosPersonales = new javax.swing.JMenu();
        mUsuarioSalir = new javax.swing.JMenu();
        mUsuarioSalirPrograma = new javax.swing.JMenu();
        mEmpleados = new javax.swing.JMenu();
        mEmpleadoListaEmpleados = new javax.swing.JMenu();
        mProductos = new javax.swing.JMenu();
        mProductosListaProductos = new javax.swing.JMenu();
        mDistribuidores = new javax.swing.JMenu();
        mDistribuidoresListaDistribuidores = new javax.swing.JMenu();
        mClientes = new javax.swing.JMenu();
        mClientesListaClientes = new javax.swing.JMenu();
        mOperaciones = new javax.swing.JMenu();
        mOperacionesVerOperaciones = new javax.swing.JMenu();
        mOperacionesNuevaVenta = new javax.swing.JMenu();
        mOpciones = new javax.swing.JMenu();
        mOpcionesConfiguracion = new javax.swing.JMenu();
        mInfoSoporte = new javax.swing.JMenu();
        mInfoSoporteTecnico = new javax.swing.JMenu();
        mInfoAyuda = new javax.swing.JMenu();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("SolarisM");
        setBackground(new java.awt.Color(255, 255, 255));
        setEnabled(false);
        setFont(new java.awt.Font("SansSerif", 1, 10)); // NOI18N

        javax.swing.GroupLayout mdiSolarisLayout = new javax.swing.GroupLayout(mdiSolaris);
        mdiSolaris.setLayout(mdiSolarisLayout);
        mdiSolarisLayout.setHorizontalGroup(
            mdiSolarisLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 670, Short.MAX_VALUE)
        );
        mdiSolarisLayout.setVerticalGroup(
            mdiSolarisLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 384, Short.MAX_VALUE)
        );

        menu.setBackground(new java.awt.Color(102, 204, 0));
        menu.setForeground(new java.awt.Color(255, 255, 255));
        menu.setFont(new java.awt.Font("SansSerif", 1, 14)); // NOI18N

        mUsuario.setForeground(new java.awt.Color(255, 255, 255));
        mUsuario.setText("Usuario");

        mUsuarioDatosPersonales.setText("Datos Personales");
        mUsuarioDatosPersonales.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                mUsuarioDatosPersonalesMouseClicked(evt);
            }
        });
        mUsuario.add(mUsuarioDatosPersonales);

        mUsuarioSalir.setText("Salir");
        mUsuarioSalir.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                mUsuarioSalirMouseClicked(evt);
            }
        });
        mUsuario.add(mUsuarioSalir);

        mUsuarioSalirPrograma.setText("Salir del programa");
        mUsuarioSalirPrograma.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                mUsuarioSalirProgramaMouseClicked(evt);
            }
        });
        mUsuario.add(mUsuarioSalirPrograma);

        menu.add(mUsuario);

        mEmpleados.setForeground(new java.awt.Color(255, 255, 255));
        mEmpleados.setText("Empleados");
        mEmpleados.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                mEmpleadosMouseClicked(evt);
            }
        });

        mEmpleadoListaEmpleados.setText("Lista Empleados");
        mEmpleadoListaEmpleados.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                mEmpleadoListaEmpleadosMouseClicked(evt);
            }
        });
        mEmpleados.add(mEmpleadoListaEmpleados);

        menu.add(mEmpleados);

        mProductos.setForeground(new java.awt.Color(255, 255, 255));
        mProductos.setText("Productos");

        mProductosListaProductos.setText("Lista de Productos");
        mProductosListaProductos.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                mProductosListaProductosMouseClicked(evt);
            }
        });
        mProductos.add(mProductosListaProductos);

        menu.add(mProductos);

        mDistribuidores.setForeground(new java.awt.Color(255, 255, 255));
        mDistribuidores.setText("Distribuidores");

        mDistribuidoresListaDistribuidores.setText("Lista de Distribuidores");
        mDistribuidoresListaDistribuidores.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                mDistribuidoresListaDistribuidoresMouseClicked(evt);
            }
        });
        mDistribuidores.add(mDistribuidoresListaDistribuidores);

        menu.add(mDistribuidores);

        mClientes.setForeground(new java.awt.Color(255, 255, 255));
        mClientes.setText("Clientes");

        mClientesListaClientes.setText("Lista de clientes");
        mClientesListaClientes.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                mClientesListaClientesMouseClicked(evt);
            }
        });
        mClientes.add(mClientesListaClientes);

        menu.add(mClientes);

        mOperaciones.setForeground(new java.awt.Color(255, 255, 255));
        mOperaciones.setText("Operaciones");

        mOperacionesVerOperaciones.setText("Ver Operaciones");
        mOperacionesVerOperaciones.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                mOperacionesVerOperacionesMouseClicked(evt);
            }
        });
        mOperaciones.add(mOperacionesVerOperaciones);

        mOperacionesNuevaVenta.setText("Nueva venta");
        mOperacionesNuevaVenta.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                mOperacionesNuevaVentaMouseClicked(evt);
            }
        });
        mOperaciones.add(mOperacionesNuevaVenta);

        menu.add(mOperaciones);

        mOpciones.setForeground(new java.awt.Color(255, 255, 255));
        mOpciones.setText("Opciones");

        mOpcionesConfiguracion.setText("Configuración");
        mOpcionesConfiguracion.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                mOpcionesConfiguracionMouseClicked(evt);
            }
        });
        mOpciones.add(mOpcionesConfiguracion);

        menu.add(mOpciones);

        mInfoSoporte.setForeground(new java.awt.Color(255, 255, 255));
        mInfoSoporte.setText("Información");

        mInfoSoporteTecnico.setText("Soporte Técnico");
        mInfoSoporteTecnico.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                mInfoSoporteTecnicoKeyPressed(evt);
            }
        });
        mInfoSoporte.add(mInfoSoporteTecnico);

        mInfoAyuda.setText("Ayuda");
        mInfoSoporte.add(mInfoAyuda);

        menu.add(mInfoSoporte);

        setJMenuBar(menu);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(mdiSolaris, javax.swing.GroupLayout.Alignment.TRAILING)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(mdiSolaris, javax.swing.GroupLayout.Alignment.TRAILING)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents
    
    //entrar al formulario de productos
    private void mProductosListaProductosMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_mProductosListaProductosMouseClicked
        frms.get(1).show();            
    }//GEN-LAST:event_mProductosListaProductosMouseClicked
    //entrar al formulario de la Lista de Distribuidores
    private void mDistribuidoresListaDistribuidoresMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_mDistribuidoresListaDistribuidoresMouseClicked
        frms.get(2).show();
    }//GEN-LAST:event_mDistribuidoresListaDistribuidoresMouseClicked
    //entrar al formulario de la lista de Clientes
    private void mClientesListaClientesMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_mClientesListaClientesMouseClicked
        frms.get(3).show();
    }//GEN-LAST:event_mClientesListaClientesMouseClicked
    //entrar a ver todas las operaciones que se han realizado en la lista de Operaciones
    private void mOperacionesVerOperacionesMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_mOperacionesVerOperacionesMouseClicked
        frms.get(4).show();
    }//GEN-LAST:event_mOperacionesVerOperacionesMouseClicked
    //entrar a realizar una nueva venta por parte del empleado
    private void mOperacionesNuevaVentaMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_mOperacionesNuevaVentaMouseClicked
        frms.get(5).show();
    }//GEN-LAST:event_mOperacionesNuevaVentaMouseClicked
    //salir del programa completamente
    private void mUsuarioSalirProgramaMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_mUsuarioSalirProgramaMouseClicked
        System.exit(0);
    }//GEN-LAST:event_mUsuarioSalirProgramaMouseClicked
    //configuración del programa
    private void mOpcionesConfiguracionMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_mOpcionesConfiguracionMouseClicked
        frms.get(6).show();
    }//GEN-LAST:event_mOpcionesConfiguracionMouseClicked
    //soporte técnico
    private void mInfoSoporteTecnicoKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_mInfoSoporteTecnicoKeyPressed
        JOptionPane.showMessageDialog(null,"Llamar a: Carlos Chavez Laguna");
    }//GEN-LAST:event_mInfoSoporteTecnicoKeyPressed
    //salir y cambiar de cuenta
    private void mUsuarioSalirMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_mUsuarioSalirMouseClicked
        frms.get(0).show();
    }//GEN-LAST:event_mUsuarioSalirMouseClicked
    //datos personales del usuario
    private void mUsuarioDatosPersonalesMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_mUsuarioDatosPersonalesMouseClicked
        frms.get(7).show();
    }//GEN-LAST:event_mUsuarioDatosPersonalesMouseClicked
    //mostrar la lista de usuario y/o empleados (solo para el administrador)
    private void mEmpleadosMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_mEmpleadosMouseClicked
        
    }//GEN-LAST:event_mEmpleadosMouseClicked
    //mostrar la lista de empleados
    private void mEmpleadoListaEmpleadosMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_mEmpleadoListaEmpleadosMouseClicked
        frms.get(8).show();
    }//GEN-LAST:event_mEmpleadoListaEmpleadosMouseClicked

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(frmSolaris.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(frmSolaris.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(frmSolaris.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(frmSolaris.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        
        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {                
                new frmSolaris().setVisible(true);
                
                
            }
        });
        
        
       
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JMenu mClientes;
    private javax.swing.JMenu mClientesListaClientes;
    private javax.swing.JMenu mDistribuidores;
    private javax.swing.JMenu mDistribuidoresListaDistribuidores;
    private javax.swing.JMenu mEmpleadoListaEmpleados;
    private javax.swing.JMenu mEmpleados;
    private javax.swing.JMenu mInfoAyuda;
    private javax.swing.JMenu mInfoSoporte;
    private javax.swing.JMenu mInfoSoporteTecnico;
    private javax.swing.JMenu mOpciones;
    private javax.swing.JMenu mOpcionesConfiguracion;
    private javax.swing.JMenu mOperaciones;
    private javax.swing.JMenu mOperacionesNuevaVenta;
    private javax.swing.JMenu mOperacionesVerOperaciones;
    private javax.swing.JMenu mProductos;
    private javax.swing.JMenu mProductosListaProductos;
    private javax.swing.JMenu mUsuario;
    private javax.swing.JMenu mUsuarioDatosPersonales;
    private javax.swing.JMenu mUsuarioSalir;
    private javax.swing.JMenu mUsuarioSalirPrograma;
    private javax.swing.JDesktopPane mdiSolaris;
    private javax.swing.JMenuBar menu;
    // End of variables declaration//GEN-END:variables
}
