/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vista;
import modelo.*;
import java.util.*;
import javax.swing.table.*;
import javax.swing.*;
/**
 *
 * @author carlos
 */
public class frmListaDistribuidores extends javax.swing.JFrame {
    //mis variables
    //private DefaultTableModel _tbModel;   
    public static ArrayList<frmAgregarDistribuidor> frms= new ArrayList();
    private String _codigo="DIST";
    private String []_columns={"Código","Nombre","RUC"};
    
    //mis funciones
    public void init(){
      //crear formularios
        frmListaDistribuidores.frms.add(new frmAgregarDistribuidor());
       
    }    
    /**
     * Creates new form ListaDistribuidores
     */
    public frmListaDistribuidores() {
        initComponents();
        this.setLocationRelativeTo(null);
        init();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        btnAgregar = new javax.swing.JButton();
        txtBuscar = new javax.swing.JTextField();
        btnBuscar = new javax.swing.JButton();
        btnModificar = new javax.swing.JButton();
        btnEliminar = new javax.swing.JButton();
        pntbDistribuidores = new javax.swing.JPanel();
        lblTablaVacia = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tbDistribuidores = new javax.swing.JTable();
        cmbFiltro = new javax.swing.JComboBox();

        setTitle("Lista de Distribuidores");
        setResizable(false);
        addComponentListener(new java.awt.event.ComponentAdapter() {
            public void componentShown(java.awt.event.ComponentEvent evt) {
                formComponentShown(evt);
            }
        });
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        btnAgregar.setText("Agregar");
        btnAgregar.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnAgregarMouseClicked(evt);
            }
        });
        getContentPane().add(btnAgregar, new org.netbeans.lib.awtextra.AbsoluteConstraints(180, 10, -1, -1));

        txtBuscar.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtBuscarKeyTyped(evt);
            }
        });
        getContentPane().add(txtBuscar, new org.netbeans.lib.awtextra.AbsoluteConstraints(260, 10, 144, -1));

        btnBuscar.setText("Buscar");
        btnBuscar.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnBuscarMouseClicked(evt);
            }
        });
        btnBuscar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBuscarActionPerformed(evt);
            }
        });
        getContentPane().add(btnBuscar, new org.netbeans.lib.awtextra.AbsoluteConstraints(520, 0, 80, 43));

        btnModificar.setText("Modificar");
        btnModificar.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnModificarMouseClicked(evt);
            }
        });
        getContentPane().add(btnModificar, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 10, -1, -1));

        btnEliminar.setText("Eliminar");
        btnEliminar.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnEliminarMouseClicked(evt);
            }
        });
        getContentPane().add(btnEliminar, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 10, -1, -1));

        pntbDistribuidores.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        lblTablaVacia.setText("No hay ningún distribuidor en la lista");
        pntbDistribuidores.add(lblTablaVacia, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 180, -1, -1));

        tbDistribuidores.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Código", "Nombre", "RUC"
            }
        ));
        tbDistribuidores.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        jScrollPane1.setViewportView(tbDistribuidores);

        pntbDistribuidores.add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(3, 0, 590, 340));

        getContentPane().add(pntbDistribuidores, new org.netbeans.lib.awtextra.AbsoluteConstraints(6, 49, -1, -1));

        cmbFiltro.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Código", "Nombre", "RUC" }));
        getContentPane().add(cmbFiltro, new org.netbeans.lib.awtextra.AbsoluteConstraints(409, 10, 100, -1));

        pack();
    }// </editor-fold>//GEN-END:initComponents
    //abrir el formulario para registrar mas empresas distribuidoras
    private void btnAgregarMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnAgregarMouseClicked
        frms.get(0).setEstado("Agregar");
        frms.get(0).show();
    }//GEN-LAST:event_btnAgregarMouseClicked
    
    //cada vez que el formulario sea visible
    private void formComponentShown(java.awt.event.ComponentEvent evt) {//GEN-FIRST:event_formComponentShown
        Util.pointerGForm.setGFrm(this);//apuntar a este formulario        
        Util.pointerGForm.setGCodigo(this._codigo);//setear el código
        Util.pointerGTable.setPointerGTable(this.tbDistribuidores);//asignarle la referencia a esta tabla
        Util.pointerGTable.setGColumnsName(this._columns);//asignarle el nombre de loas columnas
        Util.reset(this.lblTablaVacia,Market.getDistribuidores().toArray(new Distribuidor[Market.getDistribuidores().size()]));
    }//GEN-LAST:event_formComponentShown
    //modificar una fila en el siguiente formulario
    private void btnModificarMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnModificarMouseClicked
        frms.get(0).setEstado("Modificar");
        Util.modificar(this.tbDistribuidores.getSelectedRow(),frms.get(0));
    }//GEN-LAST:event_btnModificarMouseClicked
    //eliminar una fila en el siguiente formulario
    private void btnEliminarMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnEliminarMouseClicked
        Util.eliminar(this.tbDistribuidores.getSelectedRow());
    }//GEN-LAST:event_btnEliminarMouseClicked
    //botón para hacer la búsqueda del dato querido
    private void btnBuscarMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnBuscarMouseClicked
         /*Util.pointerGForm.setGTag(this.cmbFiltro.getSelectedItem().toString());            
         Util.buscar(this.txtBuscar.getText().toUpperCase());*/
    }//GEN-LAST:event_btnBuscarMouseClicked

    private void btnBuscarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBuscarActionPerformed
         Util.buscar(this.txtBuscar.getText().toUpperCase(),this.cmbFiltro.getSelectedItem().toString());
    }//GEN-LAST:event_btnBuscarActionPerformed

    private void txtBuscarKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtBuscarKeyTyped
        //si se apretó la tecla enter
        if(evt.getKeyCode()==10)
            Util.buscar(this.txtBuscar.getText().toUpperCase(),this.cmbFiltro.getSelectedItem().toString());
    }//GEN-LAST:event_txtBuscarKeyTyped

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(frmListaDistribuidores.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(frmListaDistribuidores.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(frmListaDistribuidores.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(frmListaDistribuidores.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new frmListaDistribuidores().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAgregar;
    private javax.swing.JButton btnBuscar;
    private javax.swing.JButton btnEliminar;
    private javax.swing.JButton btnModificar;
    private javax.swing.JComboBox cmbFiltro;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel lblTablaVacia;
    private javax.swing.JPanel pntbDistribuidores;
    private javax.swing.JTable tbDistribuidores;
    private javax.swing.JTextField txtBuscar;
    // End of variables declaration//GEN-END:variables
}
