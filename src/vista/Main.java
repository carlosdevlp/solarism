/*
    Aquí es donde empieza todo el programa
    Programa de arranque para el resto de programas
 */
package vista;
import modelo.*;

/**
 *
 * @author carlos chavez laguna
 */
public class Main {

   
    public static void main(String[] args) {
        //llamada al formulario principal
        frmSolaris frmMain=new frmSolaris();
        frmMain.show();
        
    }
    
}
